CREATE TABLE user_authorities (
  id BIGINT IDENTITY PRIMARY KEY,
  user_id BIGINT NOT NULL,
  authority VARCHAR(50) NOT NULL
);

ALTER TABLE user_authorities
  ADD UNIQUE (user_id, authority);

CREATE TABLE users (
  id BIGINT IDENTITY  PRIMARY KEY,
  username VARCHAR(40) NOT NULL,
  hash VARCHAR(100) NOT NULL,
  enabled BOOLEAN NOT NULL,
  name VARCHAR(100) NOT NULL,
  version BIGINT NOT NULL
);

ALTER TABLE users
  ADD CONSTRAINT users_username_unique UNIQUE (username);

--CREATE UNIQUE INDEX users_username_unique ON users(username);

ALTER TABLE user_authorities
  ADD FOREIGN KEY (user_id)
  REFERENCES users (id);

INSERT INTO users(id, username, hash, enabled, name, version) VALUES (1, 'a', '$2a$10$ZpLY7U9.xBkpOX0s0NT9aObCISKplNu.ZDkSVelbBixipaLiNPIcm', true, 'Admin', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (1, 'ROLE_ADMIN');
INSERT INTO user_authorities(user_id, authority) VALUES (1, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (2, 'b', '$2a$10$l3unhpdIZw4/bl9KMta2we06Fkd5iqbQrmgh7oTc6OA1ekxPwPDEa', true, 'Bonifacion Buendia', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (2, 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name, version) VALUES (3, 'c', '$2a$10$AXBgkA6CnnPiahZvT6XHYeKHNpPgE6TyqT.f3QrT3ry4I1GB2v1wy', false, 'Cou Casado', 1);
INSERT INTO user_authorities(user_id, authority) VALUES (3, 'ROLE_USER');
