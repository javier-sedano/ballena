package com.odroid.ballena.spring;

import com.odroid.ballena.common.AppBuildProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class SpringListener {

  private static final Logger LOGGER = LoggerFactory.getLogger(SpringListener.class);

  private final AppBuildProperties appBuildProperties;

  public SpringListener(AppBuildProperties appBuildProperties) {
    this.appBuildProperties = appBuildProperties;
  }

  @EventListener
  public void handleContextRefresh(ContextRefreshedEvent event) {
    LOGGER.info("Spring context loaded for: {}", appBuildProperties);
  }
}
