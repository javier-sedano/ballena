package com.odroid.ballena.spring;

import com.odroid.ballena.common.AppConfiguration;
import com.odroid.ballena.service.impl.lorem.asdfast.AsdfastLoremGenerator;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Configuration
public class RetrofitConfiguration {

  @Bean
  public HttpLoggingInterceptor httpLoggingInterceptor() {
    var httpLoggingInterceptor = new HttpLoggingInterceptor();
    httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    return httpLoggingInterceptor;
  }

  @Bean
  public OkHttpClient okHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
    return new OkHttpClient.Builder()
            .addNetworkInterceptor(httpLoggingInterceptor)
            .build();
  }

  @Bean
  public Retrofit retrofit(OkHttpClient okHttpClient, AppConfiguration appConfiguration) {
    return new Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(appConfiguration.getLoremBaseUrl())
            .addConverterFactory(JacksonConverterFactory.create())
            .build();
  }

  @Bean
  public AsdfastLoremGenerator loremGenerator(Retrofit retrofit) {
    return retrofit.create(AsdfastLoremGenerator.class);
  }

}
