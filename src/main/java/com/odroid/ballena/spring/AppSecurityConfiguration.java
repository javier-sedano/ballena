package com.odroid.ballena.spring;

import com.odroid.ballena.domain.Authority;
import com.odroid.ballena.security.LoggedUserProviderAuthenticationSuccessHandler;
import com.odroid.ballena.security.Status200LogoutSuccessHandler;
import com.odroid.ballena.security.Status401AuthenticationFailureHandler;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRequestAttributeHandler;
import org.springframework.security.web.csrf.CsrfTokenRequestHandler;
import org.springframework.security.web.csrf.XorCsrfTokenRequestAttributeHandler;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.function.Supplier;

import static com.odroid.ballena.rest.RestConstants.BASE_REST_URL;
import static com.odroid.ballena.rest.RestConstants.BASE_URL;
import static com.odroid.ballena.rest.RestConstants.BASE_WEB_URL;
import static com.odroid.ballena.rest.RestConstants.ROOT_URL;

@Configuration
@Order(2)
public class AppSecurityConfiguration {

  private static final String WEB_URL_PATTERN = BASE_WEB_URL + "**";
  private static final String PUBLIC_REST_URL_PATTERN = BASE_REST_URL + "ping/**";
  private static final String LOGIN_URL = BASE_REST_URL + "login";
  private static final String LOGOUT_URL = BASE_REST_URL + "logout";
  private static final String SESSION_COOKIE = "JSESSIONID";
  private static final String USERNAME_PARAMETER = "username";
  private static final String PASS_PARAMETER = "password";

  @Bean
  public SecurityFilterChain appSecurityFilterChain(HttpSecurity http) throws Exception {
    http.exceptionHandling(httpSecurityExceptionHandlingConfigurer -> httpSecurityExceptionHandlingConfigurer
            .authenticationEntryPoint(restAuthenticationEntryPoint())
    ).authorizeHttpRequests(authorizationManagerRequestMatcherRegistry -> authorizationManagerRequestMatcherRegistry
            .requestMatchers(ROOT_URL, BASE_URL, WEB_URL_PATTERN, PUBLIC_REST_URL_PATTERN).permitAll()
            .anyRequest().hasRole(Authority.ROLE_USER.getShortForm())
    ).formLogin(httpSecurityFormLoginConfigurer -> httpSecurityFormLoginConfigurer
            .loginProcessingUrl(LOGIN_URL)
            .usernameParameter(USERNAME_PARAMETER)
            .passwordParameter(PASS_PARAMETER) // NOSONAR
            .permitAll()
            .successHandler(restAuthenticationSuccessHandler())
            .failureHandler(restAuthenticationFailureHandler())
    ).logout(httpSecurityLogoutConfigurer -> httpSecurityLogoutConfigurer
            .logoutUrl(LOGOUT_URL)
            .deleteCookies(SESSION_COOKIE)
            .permitAll()
            .logoutSuccessHandler(logoutSuccessHandler())
    ).csrf(httpSecurityCsrfConfigurer -> httpSecurityCsrfConfigurer
            .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
            .csrfTokenRequestHandler(new SpaCsrfTokenRequestHandler())
    ).addFilterAfter(new SpaCsrfCookieFilter(), BasicAuthenticationFilter.class)
    ;
    return http.build();
  }

  // Inspired in https://docs.spring.io/spring-security/reference/servlet/exploits/csrf.html#csrf-integration-javascript-spa
  static final class SpaCsrfTokenRequestHandler extends CsrfTokenRequestAttributeHandler {
    private final CsrfTokenRequestHandler delegate = new XorCsrfTokenRequestAttributeHandler();

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, Supplier<CsrfToken> csrfToken) {
      this.delegate.handle(request, response, csrfToken);
    }

    @Override
    public String resolveCsrfTokenValue(HttpServletRequest request, CsrfToken csrfToken) {
      if (StringUtils.hasText(request.getHeader(csrfToken.getHeaderName()))) {
        return super.resolveCsrfTokenValue(request, csrfToken);
      }
      return this.delegate.resolveCsrfTokenValue(request, csrfToken);
    }
  }

  static final class SpaCsrfCookieFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
      CsrfToken csrfToken = (CsrfToken) request.getAttribute("_csrf");
      csrfToken.getToken();
      filterChain.doFilter(request, response);
    }
  }

  @Bean
  public AuthenticationEntryPoint restAuthenticationEntryPoint() {
    return new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED);
  }

  @Bean
  public AuthenticationSuccessHandler restAuthenticationSuccessHandler() {
    return new LoggedUserProviderAuthenticationSuccessHandler();
  }

  @Bean
  public AuthenticationFailureHandler restAuthenticationFailureHandler() {
    return new Status401AuthenticationFailureHandler();
  }

  @Bean
  public LogoutSuccessHandler logoutSuccessHandler() {
    return new Status200LogoutSuccessHandler();
  }

}
