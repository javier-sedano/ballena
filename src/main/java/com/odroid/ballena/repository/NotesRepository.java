package com.odroid.ballena.repository;

import com.odroid.ballena.domain.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.stream.Stream;

public interface NotesRepository extends JpaRepository<Note, Long> {

  @Query("select n from Note n "
          + "where "
          + "  ( lower(n.title) like lower(concat('%', :filter,'%')) "
          + "    or lower(n.content) like lower(concat('%', :filter, '%')) "
          + "  ) and n.user.username = :username "
          + "order by n.title ")
  Stream<Note> findNotes(@Param("filter") String filter, @Param("username") String username);
}
