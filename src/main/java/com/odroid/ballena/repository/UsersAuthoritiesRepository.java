package com.odroid.ballena.repository;

import com.odroid.ballena.domain.UserAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersAuthoritiesRepository extends JpaRepository<UserAuthority, Long> {
}
