package com.odroid.ballena.repository;

import com.odroid.ballena.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.stream.Stream;

public interface UsersRepository extends JpaRepository<User, Long> {

  User findByUsername(String username);

  @Query("select u from User u "
          + "where "
          + "  ( lower(u.username) like lower(concat('%', :filter,'%')) "
          + "    or lower(u.name) like lower(concat('%', :filter, '%')) "
          + "  ) "
          + "order by u.username")
  Stream<User> findUsers(@Param("filter") String filter);

}
