package com.odroid.ballena.common;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Component
@ConfigurationProperties(prefix = "app")
@ToString
@EqualsAndHashCode
@Getter
@Setter
@Validated
public class AppConfiguration {

  private static final Integer DEFAUT_PAGE_SIZE = 20;

  @NonNull
  private Integer pageSize = DEFAUT_PAGE_SIZE;

  @NonNull
  private String loremBaseUrl;
}
