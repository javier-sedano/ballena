package com.odroid.ballena.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Component;

@Component("appBuildProperties")
public class AppBuildProperties {

  private final BuildProperties buildProperties;

  public AppBuildProperties(@Autowired(required = false) BuildProperties buildProperties) {
    this.buildProperties = buildProperties;
  }

  public String toString() {
    if (buildProperties == null) {
      return "development version";
    }
    return buildProperties.getName() + " " + buildProperties.getVersion() + " @ " + buildProperties.getTime();
  }
}
