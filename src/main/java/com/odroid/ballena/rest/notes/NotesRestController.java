package com.odroid.ballena.rest.notes;

import com.odroid.ballena.common.AppConfiguration;
import com.odroid.ballena.service.iface.notes.DetailedNote;
import com.odroid.ballena.service.iface.notes.MinimalNote;
import com.odroid.ballena.service.iface.notes.NotesService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

import static com.odroid.ballena.rest.RestConstants.BASE_REST_URL;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(BASE_REST_URL + "notes")
public class NotesRestController {

  private final NotesService notesService;
  private final AppConfiguration appConfiguration;

  public NotesRestController(NotesService notesService, AppConfiguration appConfiguration) {
    this.notesService = notesService;
    this.appConfiguration = appConfiguration;
  }

  @GetMapping(value = "", produces = {APPLICATION_JSON_VALUE})
  public List<MinimalNote> find(
          @RequestParam(value = "filter", defaultValue = "") String filter,
          @RequestParam(value = "from", defaultValue = "0") long from,
          Principal principal
  ) {
    return notesService.findNotes(filter.replaceAll("[\n\r\t]", "_"), principal.getName(), from, appConfiguration.getPageSize());
  }

  @GetMapping(value = "/{id}", produces = {APPLICATION_JSON_VALUE})
  public DetailedNote retrieve(@PathVariable("id") Long id, Principal principal) {
    return notesService.getNote(id, principal.getName());
  }

  @PostMapping(value = "", produces = {APPLICATION_JSON_VALUE})
  public Long create(Principal principal) {
    return notesService.createNote(principal.getName());
  }

  @PutMapping(value = "", consumes = {APPLICATION_JSON_VALUE})
  public DetailedNote update(@RequestBody DetailedNote note, Principal principal) {
    notesService.updateNote(note, principal.getName());
    return notesService.getNote(note.getId(), principal.getName());
  }

  @DeleteMapping(value = "/{id}")
  public void delete(@PathVariable("id") Long id, Principal principal) {
    notesService.deleteNote(id, principal.getName());
  }

}
