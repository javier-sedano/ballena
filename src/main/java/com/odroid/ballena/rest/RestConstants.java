package com.odroid.ballena.rest;

public abstract class RestConstants {
  public static final String ROOT_URL = "/";
  public static final String BASE_URL = ROOT_URL + "ballena/";
  public static final String BASE_REST_URL = BASE_URL + "rest/";
  public static final String BASE_WEB_URL = BASE_URL + "web/";

  private RestConstants() {
  }
}
