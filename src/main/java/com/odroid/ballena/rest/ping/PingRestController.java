package com.odroid.ballena.rest.ping;

import com.odroid.ballena.common.AppBuildProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.odroid.ballena.rest.RestConstants.BASE_REST_URL;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(BASE_REST_URL + "ping/")
public class PingRestController {

  private final AppBuildProperties appBuildProperties;

  public PingRestController(AppBuildProperties appBuildProperties) {
    this.appBuildProperties = appBuildProperties;
  }

  @GetMapping(value = "ver", produces = {APPLICATION_JSON_VALUE})
  public String ver() {
    return appBuildProperties.toString();
  }
}
