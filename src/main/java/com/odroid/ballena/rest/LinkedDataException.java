package com.odroid.ballena.rest;

import com.odroid.ballena.service.iface.errors.AppException;

public class LinkedDataException extends AppException {

  private static final String I18N = "common.errorI18nCode.linkedData";

  public LinkedDataException(Throwable t) {
    super(I18N, t);
  }
}
