package com.odroid.ballena.service.iface.errors;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;
import java.util.Map;

@RequiredArgsConstructor
@Getter
public class ErrorDetails implements Serializable {

  @NonNull
  private final String i18nCode;
  @NonNull
  private final Map<String, Serializable> args;
}
