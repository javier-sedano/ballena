package com.odroid.ballena.service.iface.notes;

import com.odroid.ballena.domain.Note;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DetailedNote {

  @NonNull
  private Long id;

  @NonNull
  private String title;

  @NonNull
  private String content;

  @NonNull
  private Long version;

  public static DetailedNote fromNote(Note note) {
    return builder()
            .id(note.getId())
            .title(note.getTitle())
            .content(note.getContent())
            .version(note.getVersion())
            .build();
  }

}
