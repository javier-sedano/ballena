package com.odroid.ballena.service.iface.errors;

import lombok.Getter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Getter
public class AppException extends RuntimeException {
  private final ErrorDetails errorDetails;

  public AppException(String i18nCode) {
    this(i18nCode, new HashMap<>());
  }

  public AppException(String i18nCode, Throwable t) {
    this(i18nCode, new HashMap<>(), t);
  }

  public AppException(String i18nCode, Map<String, Serializable> args) {
    super(i18nCode);
    this.errorDetails = new ErrorDetails(i18nCode, args);
  }

  public AppException(String i18nCode, Map<String, Serializable> args, Throwable t) {
    super(i18nCode, t);
    this.errorDetails = new ErrorDetails(i18nCode, args);
  }

}
