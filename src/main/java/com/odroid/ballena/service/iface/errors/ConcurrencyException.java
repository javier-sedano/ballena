package com.odroid.ballena.service.iface.errors;

public class ConcurrencyException extends AppException {

  private static final String I18N = "common.errorI18nCode.concurrency";

  public ConcurrencyException() {
    super(I18N);
  }
}
