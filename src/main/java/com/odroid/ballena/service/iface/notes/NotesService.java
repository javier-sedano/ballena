package com.odroid.ballena.service.iface.notes;

import java.util.List;

public interface NotesService {

  List<MinimalNote> findNotes(String filter, String username, long from, long limit);

  DetailedNote getNote(Long id, String username);

  Long createNote(String username);

  void updateNote(DetailedNote note, String username);

  void deleteNote(Long id, String username);

}
