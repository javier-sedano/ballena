package com.odroid.ballena.service.iface.users;

import com.odroid.ballena.domain.User;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DetailedUser {

  @NonNull
  private Long id;

  @NonNull
  private String username;

  @NonNull
  private String hash;

  @NonNull
  private Boolean enabled;

  @NonNull
  private String name;

  @NonNull
  private Long version;

  @NonNull
  private Boolean admin;

  public static DetailedUser fromUser(User user) {
    return builder()
            .id(user.getId())
            .username(user.getUsername())
            .hash(user.getHash())
            .enabled(user.getEnabled())
            .name(user.getName())
            .version(user.getVersion())
            .admin(user.isAdmin())
            .build();
  }

}
