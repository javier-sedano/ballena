package com.odroid.ballena.service.iface.notes;

import com.odroid.ballena.domain.Note;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class MinimalNote {

  @NonNull
  private Long id;

  @NonNull
  private String title;

  public static MinimalNote fromNote(Note note) {
    return builder().id(note.getId()).title(note.getTitle()).build();
  }

}
