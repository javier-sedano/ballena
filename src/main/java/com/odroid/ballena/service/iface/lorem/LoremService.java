package com.odroid.ballena.service.iface.lorem;

import java.io.IOException;

public interface LoremService {
  String generateWords(Integer count) throws IOException;

  String generateParagraph(Boolean withLorem) throws IOException;
}
