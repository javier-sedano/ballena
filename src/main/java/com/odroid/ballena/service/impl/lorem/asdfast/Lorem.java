package com.odroid.ballena.service.impl.lorem.asdfast;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Lorem {

  private Integer version;

  private String text;

  private RequestParams requestParams;
}
