package com.odroid.ballena.service.impl.lorem.asdfast;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public interface AsdfastLoremGenerator {

  @GET("api/")
  @Headers("Accept: " + APPLICATION_JSON_VALUE)
  Call<Lorem> generate(@Query("type") String type, @Query("length") Integer length, @Query("startLorem") Boolean startLorem);
}
