package com.odroid.ballena.service.impl.users;


import com.odroid.ballena.domain.Authority;
import com.odroid.ballena.domain.User;
import com.odroid.ballena.domain.UserAuthority;
import com.odroid.ballena.repository.UsersAuthoritiesRepository;
import com.odroid.ballena.repository.UsersRepository;
import com.odroid.ballena.security.IsAdminPreAuthorize;
import com.odroid.ballena.service.iface.errors.ConcurrencyException;
import com.odroid.ballena.service.iface.errors.NotFoundException;
import com.odroid.ballena.service.iface.users.DetailedUser;
import com.odroid.ballena.service.iface.users.MinimalUser;
import com.odroid.ballena.service.iface.users.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional(readOnly = true)
@IsAdminPreAuthorize
public class UsersServiceJpaImpl implements UsersService {

  private static final Logger LOGGER = LoggerFactory.getLogger(UsersServiceJpaImpl.class);
  private static final String USER = "User";
  private static final String DEFAULT_NAME = "Qwerty Uiop";

  private final UsersRepository usersRepository;

  private final UsersAuthoritiesRepository usersAuthoritiesRepository;

  private final PasswordEncoder passwordEncoder;

  public UsersServiceJpaImpl(UsersRepository usersRepository, UsersAuthoritiesRepository usersAuthoritiesRepository,
                             PasswordEncoder passwordEncoder) {
    this.usersRepository = usersRepository;
    this.usersAuthoritiesRepository = usersAuthoritiesRepository;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  public List<MinimalUser> findUsers(String filter, long from, long limit) {
    LOGGER.debug("Filtering {} users from {} with {}", limit, from, filter);
    try (Stream<User> usersStream = usersRepository.findUsers(filter)) {
      List<MinimalUser> users = usersStream
              .skip(from)
              .limit(limit)
              .map(MinimalUser::fromUser)
              .toList();
      LOGGER.debug("Filtered users: {}", users);
      return users;
    }
  }

  private User getUserOrThrow(Long id, Long version) {
    var user = getUserOrThrow(id);
    if (!version.equals(user.getVersion())) {
      throw new ConcurrencyException();
    }
    return user;
  }

  private User getUserOrThrow(Long id) {
    return usersRepository.findById(id).orElseThrow(() -> new NotFoundException(USER, id));
  }

  @Override
  public DetailedUser getUser(Long id) {
    LOGGER.debug("Getting user {}", id);
    var user = DetailedUser.fromUser(getUserOrThrow(id));
    LOGGER.debug("Got user: {}", user);
    return user;
  }

  @Override
  @Transactional
  public Long createUser() {
    LOGGER.debug("Creating user");
    var user = User.builder()
            .username("0_" + UUID.randomUUID().toString())
            .hash(passwordEncoder.encode(UUID.randomUUID().toString()))
            .enabled(false)
            .name(DEFAULT_NAME)
            .build();
    user.getUserAuthorities().add(UserAuthority.builder().user(user).authority(Authority.ROLE_USER).build());
    user = usersRepository.save(user);
    Long userId = user.getId();
    LOGGER.debug("Created user {}", userId);
    return userId;
  }

  @PersistenceContext
  private EntityManager em;

  @Override
  @Transactional
  public void updateUser(DetailedUser detailedUser) {
    var user = getUserOrThrow(detailedUser.getId(), detailedUser.getVersion());
    user.setUsername(detailedUser.getUsername());
    user.setName(detailedUser.getName());
    user.setEnabled(detailedUser.getEnabled());
    if (!user.getHash().equals(detailedUser.getHash())) {
      user.setHash(passwordEncoder.encode(detailedUser.getHash()));
    }
    if (Boolean.TRUE.equals(detailedUser.getAdmin())) {
      if (!user.isAdmin()) {
        user.getUserAuthorities().add(UserAuthority.builder().user(user).authority(Authority.ROLE_ADMIN).build());
      }
    } else {
      if (user.isAdmin()) {
        var adminUserAuthority =
                user.getUserAuthorities().stream()
                        .filter(userAuthority -> userAuthority.getAuthority().equals(Authority.ROLE_ADMIN))
                        .findFirst().orElseThrow(IllegalStateException::new);
        user.getUserAuthorities().remove(adminUserAuthority);
        usersAuthoritiesRepository.delete(adminUserAuthority);
      }
    }
  }

  @Override
  @Transactional
  public void deleteUser(Long id) {
    LOGGER.debug("Deleting user {}", id);
    var user = getUserOrThrow(id);
    usersRepository.delete(user);
    LOGGER.debug("Deleted user {}", id);
  }
}
