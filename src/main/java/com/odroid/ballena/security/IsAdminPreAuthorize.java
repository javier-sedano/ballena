package com.odroid.ballena.security;

import com.odroid.ballena.domain.Authority;
import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("hasRole('" + Authority.ROLE_ADMIN_STRING + "')")
public @interface IsAdminPreAuthorize {
}
