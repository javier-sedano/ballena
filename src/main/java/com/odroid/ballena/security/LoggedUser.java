package com.odroid.ballena.security;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class LoggedUser {
  @NonNull
  String username;
  @NonNull
  List<String> roles;
}
