package com.odroid.ballena.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@Slf4j
public class LoggedUserProviderAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

  private final ObjectMapper objectMapper = new ObjectMapper();

  @Override
  public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
    var user = (User) authentication.getPrincipal();
    log.debug("Authenticated {}", user);
    List<String> roles = user.getAuthorities()
            .stream()
            .map(GrantedAuthority::getAuthority)
            .toList();
    var loggedUser = new LoggedUser(user.getUsername(), roles);
    var loggedUserJson = objectMapper.writeValueAsString(loggedUser);
    try (OutputStream os = response.getOutputStream()) {
      os.write(loggedUserJson.getBytes());
    }
  }
}
