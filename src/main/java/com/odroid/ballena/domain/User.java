package com.odroid.ballena.domain;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NotFound;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@SuppressWarnings("javaarchitecture:S7027")
public class User implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column
  @NonNull
  private String username;

  @Column
  @NonNull
  private String hash;

  @Column
  @NonNull
  private Boolean enabled;

  @Column
  @NonNull
  private String name;

  @Version
  @NonNull
  @Setter(AccessLevel.NONE)
  private Long version;

  @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @ToString.Exclude
  @EqualsAndHashCode.Exclude
  private List<UserAuthority> userAuthorities = new ArrayList<>();

  @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
  @ToString.Exclude
  @EqualsAndHashCode.Exclude
  private List<Note> notes = new ArrayList<>();

  @Builder
  private User(String username, String hash, Boolean enabled, String name, Long version) {
    this.username = username;
    this.hash = hash;
    this.enabled = enabled;
    this.name = name;
    this.version = version;
    this.userAuthorities = new ArrayList<>();
    this.notes = new ArrayList<>();
  }

  public boolean isAdmin() {
    long adminCount = getUserAuthorities().stream().filter(userAuthority -> userAuthority.getAuthority().equals(Authority.ROLE_ADMIN)).count();
    return (adminCount > 0);
  }
}
