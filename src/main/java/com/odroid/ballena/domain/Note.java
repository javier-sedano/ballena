package com.odroid.ballena.domain;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import java.io.Serializable;

@Entity
@Table(name = "notes")
@Data
@NoArgsConstructor
@SuppressWarnings("javaarchitecture:S7027")
public class Note implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column
  @NonNull
  private String title;

  @Column
  @NonNull
  private String content;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id", referencedColumnName = "id")
  @NonNull
  private User user;

  @Version
  @NonNull
  @Setter(AccessLevel.NONE)
  private Long version;

  @Builder
  private Note(String title, String content, User user, Long version) {
    this.title = title;
    this.content = content;
    this.user = user;
    this.version = version;
  }
}
