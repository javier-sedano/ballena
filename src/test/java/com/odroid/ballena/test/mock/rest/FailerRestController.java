package com.odroid.ballena.test.mock.rest;

import com.odroid.ballena.test.mock.service.iface.FailerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.odroid.ballena.rest.RestConstants.BASE_REST_URL;

@RestController
@RequestMapping(BASE_REST_URL + "ping/")
public class FailerRestController {

  private final FailerService failerService;

  public FailerRestController(FailerService failerService) {
    this.failerService = failerService;
  }

  @GetMapping("fail")
  public void fail() {
    failerService.fail();
  }

}
