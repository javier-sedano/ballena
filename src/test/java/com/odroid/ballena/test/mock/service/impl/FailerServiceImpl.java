package com.odroid.ballena.test.mock.service.impl;

import com.odroid.ballena.test.mock.service.iface.FailerService;
import org.springframework.stereotype.Service;

@Service
public class FailerServiceImpl implements FailerService {
  @Override
  public void fail() {
    throw new FailOnPurposeException("On purpose");
  }
}
