package com.odroid.ballena.test.mock.rest.privatee;

import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

import static com.odroid.ballena.rest.RestConstants.BASE_REST_URL;

@RestController
@RequestMapping(BASE_REST_URL + "private/")
public class PrivateRestController {
  @GetMapping(value = "me", produces = MimeTypeUtils.TEXT_PLAIN_VALUE)
  @SuppressWarnings("javasecurity:S5131")
  public String whoAmI(Principal principal) {
    return principal.getName();
  }
}
