package com.odroid.ballena.test.api;

public abstract class ApiTestConstants {
  public static final String ROOT_URL = "/";
  public static final String BASE_URL = ROOT_URL + "ballena/";
  public static final String BASE_REST_URL = BASE_URL + "rest/";
  public static final String BASE_WEB_URL = BASE_URL + "web/";

  public static final String ROLE_ADMIN_SHORT = "ADMIN";
  public static final String ROLE_USER_SHORT = "USER";

  private ApiTestConstants() {
    // Intentionally blank to avoid instantiation
  }
}
