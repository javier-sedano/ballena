package com.odroid.ballena.test.api;

import com.odroid.ballena.test.AppTestPropertySource;
import com.odroid.ballena.SpringBootApp;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static com.odroid.ballena.test.api.ApiTestConstants.BASE_REST_URL;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = SpringBootApp.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AppTestPropertySource
@Slf4j
class ErrorApiTest {

  @Autowired
  private TestRestTemplate template;

  @Test
  void testFail() throws Exception {
    ResponseEntity<String> response = template.getForEntity(BASE_REST_URL + "ping/fail", String.class);
    log.debug("Fail received: {}", response);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    JSONAssert.assertEquals("{\"i18nCode\":\"error.backend.onPurpose\",\"args\":{\"reason\":\"On purpose\",\"code\":\"qwerty\"}}", response.getBody(), true);
  }

}
