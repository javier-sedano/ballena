package com.odroid.ballena.test.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.odroid.ballena.test.AppTestPropertySource;
import com.odroid.ballena.SpringBootApp;
import com.odroid.ballena.security.LoggedUser;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.odroid.ballena.test.api.ApiTestConstants.BASE_REST_URL;
import static com.odroid.ballena.domain.Authority.ROLE_ADMIN;
import static com.odroid.ballena.domain.Authority.ROLE_USER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = SpringBootApp.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AppTestPropertySource
@Slf4j
class LoginApiTest {

  private static final String PUBLIC_URL = BASE_REST_URL + "ping/ver";
  private static final String LOGIN_URL = BASE_REST_URL + "login";
  private static final String LOGOUT_URL = BASE_REST_URL + "logout";
  private static final String PRIVATE_URL = BASE_REST_URL + "private/me";
  private static final String SET_COOKIE_HEADER = "Set-Cookie";
  private static final String XSRF_TOKEN_COOKIE = "XSRF-TOKEN";
  private static final String X_XSRF_TOKEN_HEADER = "X-XSRF-TOKEN";
  private static final String A_USER = "a";
  private static final String A_PASSWORD = "a";
  private static final String B_USER = "b";
  private static final String B_PASSWORD = "b";
  private static final String JSESSIONID = "JSESSIONID";

  private final ObjectMapper jackson = new ObjectMapper();

  @Autowired
  private TestRestTemplate template;

  Map<String, HttpCookie> mergeCookies(Map<String, HttpCookie> cookies, List<String> setCookies) {
    log.debug("Merging cookies: {} to: {}", setCookies, cookies);
    Map<String, HttpCookie> newCookies = new HashMap<>(cookies);
    for (String setCookie : Optional.ofNullable(setCookies).orElse(new ArrayList<>())) {
      for (HttpCookie httpCookie : HttpCookie.parse(setCookie)) {
        if (httpCookie.hasExpired()) {
          newCookies.remove(httpCookie.getName());
        } else {
          newCookies.put(httpCookie.getName(), httpCookie);
        }
      }
    }
    return newCookies;
  }

  private Map<String, HttpCookie> testVersionAndReturnCookies() {
    ResponseEntity<String> response = template.getForEntity(PUBLIC_URL, String.class);
    log.debug("Version received: {}", response.toString());
    assertFalse(response.getBody().isEmpty());
    assertEquals(HttpStatus.OK, response.getStatusCode());
    return mergeCookies(new HashMap<>(), response.getHeaders().get(SET_COOKIE_HEADER));
  }

  private void addCookiesHeaders(HttpHeaders headers, Map<String, HttpCookie> cookies) {
    log.debug("Current cookies: {}", cookies);
    StringBuilder value = new StringBuilder();
    for (HttpCookie cookie : cookies.values()) {
      String oneValue = cookie.getName() + "=" + cookie.getValue() + "; ";
      value.append(oneValue);
    }
    headers.set("Cookie", value.toString());
  }

  @Test
  void testPublicApi() {
    testVersionAndReturnCookies();
  }

  @Test
  void testPrivateApiWhenNotAuthenticated() {
    ResponseEntity<String> response = template.getForEntity(PRIVATE_URL, String.class);
    log.debug("Me: {}", response.toString());
    assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
  }


  private ResponseEntity<String> postToLogin(String username, String password, Map<String, HttpCookie> cookies, HeadersCallback headersCallback) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    headersCallback.addHeaders(headers, cookies);
    MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
    map.add("username", username);
    map.add("password", password);
    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
    log.debug("Login sending: {}", request.toString());
    ResponseEntity<String> response = template.postForEntity(LOGIN_URL, request, String.class);
    log.debug("Login received: {}", response.toString());
    return response;
  }

  private ResponseEntity<String> postToLogin(String username, String password, Map<String, HttpCookie> cookies) {
    return postToLogin(username, password, cookies,
            (headers, cookies1) -> {
              addCookiesHeaders(headers, cookies);
              headers.set(X_XSRF_TOKEN_HEADER, cookies.get(XSRF_TOKEN_COOKIE).getValue());
            }
    );
  }

  private Map<String, HttpCookie> testLoginAndReturnCookies(String username, String password, String... expectedRoles) throws IOException {
    Map<String, HttpCookie> cookies = testVersionAndReturnCookies();
    ResponseEntity<String> response = postToLogin(username, password, cookies);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    String loggedUserJson = response.getBody();
    LoggedUser loggedUser = jackson.readValue(loggedUserJson, LoggedUser.class);
    assertEquals(username, loggedUser.getUsername());
    List<String> roles = loggedUser.getRoles();
    assertEquals(expectedRoles.length, roles.size());
    for (String expectedRol : expectedRoles) {
      assertTrue(roles.contains(expectedRol));
    }
    return mergeCookies(cookies, response.getHeaders().get(SET_COOKIE_HEADER));
  }

  @Test
  void testLoginA() throws IOException {
    testLoginAndReturnCookies(A_USER, A_PASSWORD, ROLE_ADMIN.name(), ROLE_USER.name());
  }

  @Test
  void testLoginB() throws IOException {
    testLoginAndReturnCookies(B_USER, B_PASSWORD, ROLE_USER.name());
  }

  @Test
  void testLoginCDisabled() {
    Map<String, HttpCookie> cookies = testVersionAndReturnCookies();
    ResponseEntity<String> response = postToLogin("c", "c", cookies);
    assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
  }

  public Map<String, HttpCookie> testLoginAndPrivateApiAndReturnCookies() throws IOException {
    Map<String, HttpCookie> cookies = testLoginAndReturnCookies(A_USER, A_PASSWORD, ROLE_ADMIN.name(), ROLE_USER.name());
    HttpHeaders headers = new HttpHeaders();
    addCookiesHeaders(headers, cookies);
    HttpEntity<Void> request = new HttpEntity<>(headers);
    log.debug("Me sending: {}", request);
    ResponseEntity<String> response = template.exchange(PRIVATE_URL, HttpMethod.GET, request, String.class);
    log.debug("Me received: {}", response.toString());
    assertEquals(HttpStatus.OK, response.getStatusCode());
    return mergeCookies(cookies, response.getHeaders().get(SET_COOKIE_HEADER));
  }

  @Test
  void testLoginAndPrivateApi() throws IOException {
    testLoginAndPrivateApiAndReturnCookies();
  }

  @Test
  void testLoginAndLogout() throws IOException {
    testPrivateApiWhenNotAuthenticated();
    Map<String, HttpCookie> cookies = testLoginAndPrivateApiAndReturnCookies();
    HttpHeaders headers = new HttpHeaders();
    addCookiesHeaders(headers, cookies);
    headers.set(X_XSRF_TOKEN_HEADER, cookies.get(XSRF_TOKEN_COOKIE).getValue());
    HttpEntity<Void> request = new HttpEntity<>(headers);
    log.debug("Logout sending: {}", request.toString());
    ResponseEntity<String> response = template.postForEntity(LOGOUT_URL, request, String.class);
    log.debug("Logout received: {}", response.toString());
    assertEquals(HttpStatus.OK, response.getStatusCode());
  }

  @Test
  void testLoginWithWrongCredentials() {
    Map<String, HttpCookie> cookies = testVersionAndReturnCookies();
    ResponseEntity<String> response = postToLogin(A_USER, "wrong-" + A_PASSWORD, cookies);
    log.debug(response.toString());
    assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
  }

  @Test
  void testLoginWithoutCsrfAtAll() {
    Map<String, HttpCookie> cookies = testVersionAndReturnCookies();
    ResponseEntity<String> response = postToLogin(A_USER, A_PASSWORD, cookies,
            (callbackHeaders, callbackCookies) -> {
              log.debug("Removed CSRF cookie and header not added");
              callbackCookies.remove(XSRF_TOKEN_COOKIE);
              addCookiesHeaders(callbackHeaders, callbackCookies);
            }
    );
    assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
  }

  @Test
  void testLoginWithoutCsrfHeader() {
    Map<String, HttpCookie> cookies = testVersionAndReturnCookies();
    ResponseEntity<String> response = postToLogin(A_USER, A_PASSWORD, cookies,
            (callbackHeaders, callbackCookies) -> {
              log.debug("CSRF header not added");
              addCookiesHeaders(callbackHeaders, callbackCookies);
            }
    );
    assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
  }

  @Test
  void testLoginWithWrongCsrf() {
    Map<String, HttpCookie> cookies = testVersionAndReturnCookies();
    ResponseEntity<String> response = postToLogin(A_USER, A_PASSWORD, cookies,
            (callbackHeaders, callbackCookies) -> {
              log.debug("Wrong CSRF header not added");
              addCookiesHeaders(callbackHeaders, callbackCookies);
              callbackHeaders.set(X_XSRF_TOKEN_HEADER, "wrongcsrf");
            }
    );
    assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
  }

  @Test
  void testLoginAndPrivateApiWithWrongSession() throws IOException {
    Map<String, HttpCookie> cookies = testLoginAndReturnCookies(A_USER, A_PASSWORD, ROLE_ADMIN.name(), ROLE_USER.name());
    HttpCookie session = cookies.get(JSESSIONID);
    session.setValue("wrongsession");
    cookies.put(JSESSIONID, session);
    HttpHeaders headers = new HttpHeaders();
    addCookiesHeaders(headers, cookies);
    HttpEntity<Void> request = new HttpEntity<>(headers);
    log.debug("Me sending: {}", request);
    ResponseEntity<String> response = template.exchange(PRIVATE_URL, HttpMethod.GET, request, String.class);
    log.debug("Me received: {}", response.toString());
    assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
  }

  @FunctionalInterface
  private interface HeadersCallback {
    void addHeaders(HttpHeaders headers, Map<String, HttpCookie> cookies);
  }
}
