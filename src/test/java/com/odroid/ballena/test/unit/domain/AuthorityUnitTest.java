package com.odroid.ballena.test.unit.domain;

import com.odroid.ballena.domain.Authority;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AuthorityUnitTest {

  @Test
  void testAuthorityStringsMatchNames() {
    assertEquals(2, Authority.values().length);
    assertEquals(Authority.ROLE_USER_STRING, Authority.ROLE_USER.toString());
    assertEquals(Authority.ROLE_ADMIN_STRING, Authority.ROLE_ADMIN.toString());
  }
}
