package com.odroid.ballena.test.unit.common;

import com.odroid.ballena.common.AppBuildProperties;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.info.BuildProperties;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class AppBuildPropertiesUnitTest {

  @Mock
  private BuildProperties buildProperties;

  private AutoCloseable mockitoMocks;

  @BeforeEach
  void initMocks() {
    mockitoMocks = MockitoAnnotations.openMocks(this);
  }

  @AfterEach
  void closeMocks() throws Exception {
    mockitoMocks.close();
  }

  @Test
  void testEmpty() {
    AppBuildProperties appBuildProperties = new AppBuildProperties(null);
    String info = appBuildProperties.toString();
    assertEquals("development version", info);
  }

  @Test
  void testFilled() {
    Instant now = Instant.now();
    when(buildProperties.getName()).thenReturn("Rafael");
    when(buildProperties.getVersion()).thenReturn("666");
    when(buildProperties.getTime()).thenReturn(now);
    AppBuildProperties appBuildProperties = new AppBuildProperties(buildProperties);
    String info = appBuildProperties.toString();
    assertEquals("Rafael 666 @ " + now, info);
  }
}
