package com.odroid.ballena.test.unit.rest.users;

import com.odroid.ballena.common.AppConfiguration;
import com.odroid.ballena.rest.users.UsernameCollisionException;
import com.odroid.ballena.rest.users.UsersRestController;
import com.odroid.ballena.service.iface.users.UsersService;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;

class UsersRestControllerUnitTest {

  private static final String USERNAME_CONSTRAINT = "USERS_USERNAME_UNIQUE";
  private static final String ANOTHER_CONSTRAINT = "ANOTHER CONSTRAINT";

  @Mock
  private UsersService usersService;

  @Mock
  private AppConfiguration appConfiguration;

  private UsersRestController usersController;

  private AutoCloseable mockitoMocks;

  @BeforeEach
  void initObjects() {
    mockitoMocks = MockitoAnnotations.openMocks(this);
    usersController = new UsersRestController(usersService, appConfiguration);
  }

  @AfterEach
  void closeMocks() throws Exception {
    mockitoMocks.close();
  }

  @Test
  void testUpdateArbitraryException() {
    doThrow(new NullPointerException()).when(usersService).updateUser(any());
    assertThrows(NullPointerException.class, () -> usersController.update(null));
  }

  @Test
  void testUpdateDataIntegrityViolationExceptionWhichIsNotConstraintViolationException() {
    doThrow(new DataIntegrityViolationException("pffff")).when(usersService).updateUser(any());
    assertThrows(DataIntegrityViolationException.class, () -> usersController.update(null));
  }

  @Test
  void testUpdateDataIntegrityViolationExceptionWithConstraintViolationExceptionWhichIsNotUsername() {
    doThrow(new DataIntegrityViolationException("pffff", new ConstraintViolationException("agg", null, ANOTHER_CONSTRAINT))).when(usersService).updateUser(any());
    assertThrows(DataIntegrityViolationException.class, () -> usersController.update(null));
  }

  @Test
  void testUpdateDataIntegrityViolationExceptionWithConstraintViolationExceptionWhichIsUsername() {
    doThrow(new DataIntegrityViolationException("pffff", new ConstraintViolationException("agg", null, USERNAME_CONSTRAINT))).when(usersService).updateUser(any());
    assertThrows(UsernameCollisionException.class, () -> usersController.update(null));
  }

}
