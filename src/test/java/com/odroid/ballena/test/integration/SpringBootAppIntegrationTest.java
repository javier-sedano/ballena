package com.odroid.ballena.test.integration;

import com.odroid.ballena.test.AppTestPropertySource;
import com.odroid.ballena.SpringBootApp;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = SpringBootApp.class)
@AppTestPropertySource
class SpringBootAppIntegrationTest {

  @Autowired
  ApplicationContext applicationContext;

  @Test
  void contextLoads() {
    assertNotNull(applicationContext);
  }
}
