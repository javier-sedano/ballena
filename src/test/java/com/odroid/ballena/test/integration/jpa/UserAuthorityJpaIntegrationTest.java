package com.odroid.ballena.test.integration.jpa;

import com.odroid.ballena.test.AppTestPropertySource;
import com.odroid.ballena.SpringBootApp;
import com.odroid.ballena.domain.Authority;
import com.odroid.ballena.domain.User;
import com.odroid.ballena.domain.UserAuthority;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = SpringBootApp.class)
@AppTestPropertySource
class UserAuthorityJpaIntegrationTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserAuthorityJpaIntegrationTest.class);

  private static final String AUSER_USERNAME = "a";

  @PersistenceContext
  private EntityManager em;

  @Autowired
  private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  @Test
  @Transactional
  void testMinimalAdmins() {
    List<UserAuthority> adminsUserAuthorities = em.createQuery(
            "from UserAuthority a where a.authority = :authority", UserAuthority.class)
            .setParameter("authority", Authority.ROLE_ADMIN)
            .getResultList();
    LOGGER.debug("Admins: {}", adminsUserAuthorities);
    assertTrue(adminsUserAuthorities.stream().map(UserAuthority::getUser).map(User::getUsername).anyMatch(AUSER_USERNAME::equals));
  }

  @Test
  @Transactional
  void testAuthority() {
    User xUser = User.builder().username("x").hash("pwd").enabled(true).name("Xp").build();
    em.persist(xUser);
    xUser.getUserAuthorities().add(UserAuthority.builder().user(xUser).authority(Authority.ROLE_USER).build());
    em.flush();
    String role = namedParameterJdbcTemplate.queryForObject(
            "SELECT authority FROM user_authorities JOIN users ON user_authorities.user_id = users.id WHERE users.username = 'x'",
            new EmptySqlParameterSource(),
            String.class
    );
    assertEquals(Authority.ROLE_USER.name(), role);
  }

}
