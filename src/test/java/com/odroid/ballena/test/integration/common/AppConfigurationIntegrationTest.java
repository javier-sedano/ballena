package com.odroid.ballena.test.integration.common;

import com.odroid.ballena.test.AppTestPropertySource;
import com.odroid.ballena.SpringBootApp;
import com.odroid.ballena.common.AppConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = SpringBootApp.class)
@AppTestPropertySource
class AppConfigurationIntegrationTest {

  @Autowired
  private AppConfiguration appConfiguration;

  @Test
  void testPageSize() {
    assertEquals(20, (int) appConfiguration.getPageSize());
  }

}
