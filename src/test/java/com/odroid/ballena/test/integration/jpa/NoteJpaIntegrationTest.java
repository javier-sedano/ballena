package com.odroid.ballena.test.integration.jpa;

import com.odroid.ballena.SpringBootApp;
import com.odroid.ballena.domain.Note;
import com.odroid.ballena.domain.User;
import com.odroid.ballena.test.AppTestPropertySource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest(classes = SpringBootApp.class)
@AppTestPropertySource
class NoteJpaIntegrationTest {

  private static final String NOTE_TITLE = "T1tl3";
  private static final String NOTE_CONTENT = "C0nt3nt";
  private static final Long AUSER_ID = 1L;

  @PersistenceContext
  private EntityManager em;

  @Test
  @Transactional
  void testSimpleJpaOperations() {
    User aUser = em.find(User.class, AUSER_ID);
    Note note1 = Note.builder().title(NOTE_TITLE).content(NOTE_CONTENT).user(aUser).build();
    em.persist(note1);
    Note gotNote1 = em.find(Note.class, note1.getId());
    assertEquals(NOTE_TITLE, gotNote1.getTitle());
    assertEquals(NOTE_CONTENT, gotNote1.getContent());
    assertEquals(note1.getId(), gotNote1.getId());
    assertEquals(AUSER_ID, gotNote1.getUser().getId());
  }
}
