package com.odroid.ballena.test.integration.rest.users;

import com.odroid.ballena.SpringBootApp;
import com.odroid.ballena.common.AppConfiguration;
import com.odroid.ballena.domain.Authority;
import com.odroid.ballena.domain.Note;
import com.odroid.ballena.domain.User;
import com.odroid.ballena.rest.LinkedDataException;
import com.odroid.ballena.rest.users.UsernameCollisionException;
import com.odroid.ballena.rest.users.UsersRestController;
import com.odroid.ballena.service.iface.errors.ConcurrencyException;
import com.odroid.ballena.service.iface.errors.NotFoundException;
import com.odroid.ballena.service.iface.users.DetailedUser;
import com.odroid.ballena.service.iface.users.MinimalUser;
import com.odroid.ballena.test.AppTestPropertySource;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.odroid.ballena.test.TestConstants.ID_ADMIN;
import static com.odroid.ballena.test.TestConstants.USERNAME_ADMIN;
import static com.odroid.ballena.test.integration.IntegrationTestUtils.MINIMAL_USER_A;
import static com.odroid.ballena.test.integration.IntegrationTestUtils.MINIMAL_USER_B;
import static com.odroid.ballena.test.integration.IntegrationTestUtils.MINIMAL_USER_C;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = SpringBootApp.class)
@AppTestPropertySource
@WithMockUser(authorities = {Authority.ROLE_USER_STRING, Authority.ROLE_ADMIN_STRING})
class UsersRestControllerIntegrationTest {

  @PersistenceContext
  private EntityManager em;

  @Autowired
  private UsersRestController usersController;

  @Autowired
  private AppConfiguration appConfiguration;

  @Autowired
  private TransactionTemplate transactionTemplate;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Test
  @Transactional
  void testFindUsersFilter() {
    assertEquals(
            Arrays.asList(MINIMAL_USER_A, MINIMAL_USER_B, MINIMAL_USER_C),
            usersController.find("", 0));
    User user1 = User.builder().username("qwer").hash("").enabled(true).name("tyuiop").build();
    em.persist(user1);
    User user2 = User.builder().username("asdf").hash("").enabled(true).name("ghjkl").build();
    em.persist(user2);
    User user3 = User.builder().username("ASDF").hash("").enabled(true).name("GHJKL").build();
    em.persist(user3);
    User user4 = User.builder().username("asdf2").hash("").enabled(true).name("ghjkl").build();
    em.persist(user4);

    assertEquals(
            new ArrayList<>(),
            usersController.find("zxcvbnm", 0)
    );
    assertEquals(
            Collections.singletonList(MinimalUser.fromUser(user1)),
            usersController.find("q", 0)
    );
    assertEquals(
            Collections.singletonList(MinimalUser.fromUser(user1)),
            usersController.find("r", 0)
    );
    assertEquals(
            Collections.singletonList(MinimalUser.fromUser(user1)),
            usersController.find("we", 0)
    );
    assertEquals(
            Collections.singletonList(MinimalUser.fromUser(user1)),
            usersController.find("t", 0)
    );
    assertEquals(
            Collections.singletonList(MinimalUser.fromUser(user1)),
            usersController.find("p", 0)
    );
    assertEquals(
            Collections.singletonList(MinimalUser.fromUser(user1)),
            usersController.find("ui", 0)
    );
  }

  @Test
  @Transactional
  void testFindUsersCaseInsensitiveFilter() {
    assertEquals(
            Arrays.asList(MINIMAL_USER_A, MINIMAL_USER_B, MINIMAL_USER_C),
            usersController.find("", 0));
    User user1 = User.builder().username("qwer").hash("").enabled(true).name("tyuiop").build();
    em.persist(user1);
    User user2 = User.builder().username("asdf").hash("").enabled(true).name("ghjkl").build();
    em.persist(user2);
    User user3 = User.builder().username("ASDF").hash("").enabled(true).name("GHJKL").build();
    em.persist(user3);
    User user4 = User.builder().username("asdf2").hash("").enabled(true).name("ghjkl").build();
    em.persist(user4);

    assertEquals(
            Arrays.asList(MinimalUser.fromUser(user3), MinimalUser.fromUser(user2), MinimalUser.fromUser(user4)),
            usersController.find("asdf", 0)
    );
    assertEquals(
            Arrays.asList(MinimalUser.fromUser(user3), MinimalUser.fromUser(user2), MinimalUser.fromUser(user4)),
            usersController.find("ASDF", 0)
    );
    assertEquals(
            Arrays.asList(MinimalUser.fromUser(user3), MinimalUser.fromUser(user2), MinimalUser.fromUser(user4)),
            usersController.find("ghjkl", 0)
    );
    assertEquals(
            Arrays.asList(MinimalUser.fromUser(user3), MinimalUser.fromUser(user2), MinimalUser.fromUser(user4)),
            usersController.find("GHJKL", 0)
    );
  }

  @Test
  @Transactional
  void testFindUsersWithPagination() {
    assertEquals(
            Arrays.asList(MINIMAL_USER_A, MINIMAL_USER_B, MINIMAL_USER_C),
            usersController.find("", 0));
    List<MinimalUser> insertedMinimalUsers = new ArrayList<>();
    for (int i = 0; i < 10 * appConfiguration.getPageSize(); i++) {
      User user = User.builder().username(String.format("user%03d", i)).hash("").enabled(true).name(String.format("name%03d", i)).build();
      em.persist(user);
      insertedMinimalUsers.add(MinimalUser.fromUser(user));
    }
    assertEquals(
            insertedMinimalUsers.subList(0, appConfiguration.getPageSize()),
            usersController.find("user", 0)
    );
    assertEquals(
            insertedMinimalUsers.subList(appConfiguration.getPageSize(), appConfiguration.getPageSize() + appConfiguration.getPageSize()),
            usersController.find("user", appConfiguration.getPageSize())
    );
    assertEquals(
            insertedMinimalUsers.subList(insertedMinimalUsers.size() - 3, insertedMinimalUsers.size()),
            usersController.find("user", insertedMinimalUsers.size() - 3)
    );
  }

  @Test
  @Transactional
  void testGetUser() {
    User user1 = User.builder().username("qwer").hash("").enabled(true).name("tyuiop").build();
    em.persist(user1);
    DetailedUser gotDetailedUser1 = usersController.retrieve(user1.getId());
    assertEquals(DetailedUser.fromUser(user1), gotDetailedUser1);
  }

  @Test
  @Transactional
  void testGetUserNotExisting() {
    User user1 = User.builder().username("qwer").hash("").enabled(true).name("tyuiop").build();
    em.persist(user1);
    Long nonExistingUserId = user1.getId() + 1;
    assertThrows(NotFoundException.class, () -> usersController.retrieve(nonExistingUserId));
  }

  @Test
  @Transactional
  void testCreate() {
    Long userId = usersController.create();
    User gotUser = em.find(User.class, userId);
    assertTrue(gotUser.getUsername().startsWith("0_"));
    assertFalse(gotUser.getHash().isEmpty());
    assertFalse(gotUser.getName().isEmpty());
    assertFalse(gotUser.getEnabled());
    assertFalse(gotUser.isAdmin());
    assertTrue(gotUser.getUserAuthorities().stream().anyMatch(userAuthority -> userAuthority.getAuthority().equals(Authority.ROLE_USER)));
  }

  @Test
  @Transactional
  void testDeleteSimplest() {
    User user = User.builder().username("qwer").hash("").enabled(true).name("tyuiop").build();
    em.persist(user);
    usersController.delete(user.getId());
    assertNull(em.find(User.class, user.getId()));
  }

  @Test
  void testDeleteWithAuthorities() {
    Long userId = usersController.create();
    try {
      assertNotNull(em.find(User.class, userId));
    } finally {
      usersController.delete(userId);
      assertNull(em.find(User.class, userId));
    }
  }

  @Test
  void testDeleteWithNotesDetected() {
    Long userId = transactionTemplate.execute(status -> {
      User user = User.builder().username("qwer").hash("").enabled(true).name("tyuiop").build();
      em.persist(user);
      Note note = Note.builder().title("t").content("c").user(user).build();
      em.persist(note);
      return user.getId();
    });
    try {
      assertNotNull(em.find(User.class, userId));
    } finally {
      try {
        assertThrows(LinkedDataException.class, () -> usersController.delete(userId));
      } finally {
        transactionTemplate.execute(status -> {
          em.createNativeQuery("DELETE FROM notes WHERE user_id = :user_id").setParameter("user_id", userId).executeUpdate();
          em.createNativeQuery("DELETE FROM users WHERE id = :id").setParameter("id", userId).executeUpdate();
          return null;
        });
      }
    }
  }

  @Test
  void testUpdateBasicData() {
    Long userId = usersController.create();
    try {
      DetailedUser user = usersController.retrieve(userId);
      user.setUsername("zxcv");
      user.setName("bnm");
      user.setEnabled(true);
      DetailedUser updatedUser = usersController.update(user);
      assertEquals("zxcv", updatedUser.getUsername());
      assertEquals("bnm", updatedUser.getName());
      assertTrue(updatedUser.getEnabled());
      assertEquals(user.getHash(), updatedUser.getHash());
    } finally {
      usersController.delete(userId);
    }
  }

  @Test
  void testUpdatePassword() {
    Long userId = usersController.create();
    try {
      DetailedUser user = usersController.retrieve(userId);
      user.setHash("p455w0rd");
      DetailedUser updatedUser = usersController.update(user);
      assertTrue(passwordEncoder.matches("p455w0rd", updatedUser.getHash()));
    } finally {
      usersController.delete(userId);
    }
  }

  @Test
  void testUpdateAdmin() {
    Long userId = usersController.create();
    try {
      DetailedUser user = usersController.retrieve(userId);
      user.setAdmin(false);
      DetailedUser updatedUser1 = usersController.update(user);
      assertFalse(updatedUser1.getAdmin());

      updatedUser1.setAdmin(true);
      DetailedUser updatedUser2 = usersController.update(updatedUser1);
      assertTrue(updatedUser2.getAdmin());

      updatedUser2.setAdmin(true);
      DetailedUser updatedUser3 = usersController.update(updatedUser2);
      assertTrue(updatedUser3.getAdmin());

      updatedUser3.setAdmin(false);
      DetailedUser updatedUser4 = usersController.update(updatedUser3);
      assertFalse(updatedUser4.getAdmin());
    } finally {
      usersController.delete(userId);
    }
  }

  @Test
  void testConcurrentUpdateDetected() {
    Long userId = usersController.create();
    try {
      DetailedUser user1 = usersController.retrieve(userId);
      DetailedUser user2 = usersController.retrieve(userId);
      user1.setName("11");
      user2.setName("22");
      usersController.update(user1);
      DetailedUser user1Modified = usersController.retrieve(userId);
      assertEquals(user1.getName(), user1Modified.getName());
      assertThrows(ConcurrencyException.class, () -> usersController.update(user2));
    } finally {
      usersController.delete(userId);
    }
  }

  @Test
  void testUpdateCollidingUsernameDetected() {
    Long userId = usersController.create();
    try {
      DetailedUser user = usersController.retrieve(userId);
      user.setUsername("a");
      assertThrows(UsernameCollisionException.class, () -> usersController.update(user));
    } finally {
      usersController.delete(userId);
    }
  }

  @Test
  @WithMockUser(authorities = {Authority.ROLE_USER_STRING})
  @Transactional
  void testCreateWithPlainUserForbidden() {
    assertThrows(AccessDeniedException.class, () -> usersController.create());
  }

  @Test
  @WithMockUser(authorities = {Authority.ROLE_USER_STRING})
  @Transactional
  void testRetrieveWithPlainUserForbidden() {
    assertThrows(AccessDeniedException.class, () -> usersController.retrieve(ID_ADMIN));
  }

  @Test
  @WithMockUser(authorities = {Authority.ROLE_USER_STRING})
  @Transactional
  void testFindWithPlainUserForbidden() {
    assertThrows(AccessDeniedException.class, () -> usersController.find("", 0));
  }

  @Test
  @WithMockUser(authorities = {Authority.ROLE_USER_STRING})
  @Transactional
  void testDeleteWithPlainUserForbidden() {
    assertThrows(AccessDeniedException.class, () -> usersController.delete(ID_ADMIN));
  }

  @Test
  @WithMockUser(authorities = {Authority.ROLE_USER_STRING})
  @Transactional
  void testUpdateWithPlainUserForbidden() {
    DetailedUser hackedAdmin = DetailedUser.builder()
            .id(ID_ADMIN)
            .username(USERNAME_ADMIN)
            .hash("pwned")
            .enabled(true)
            .name("Hacked")
            .version(1000L)
            .admin(true)
            .build();
    assertThrows(AccessDeniedException.class, () -> usersController.update(hackedAdmin));
  }

}
