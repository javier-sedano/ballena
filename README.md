Sample web application based on Spring Boot and Vue.

The architecture of this project is an obvious overkill for an application this small (a couple of windows and a couple of tables), but it is an architectural sample for larger applications.

Master branch:
[![pipeline status](https://gitlab.com/javier-sedano/ballena/badges/master/pipeline.svg)](https://gitlab.com/javier-sedano/ballena/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.ballena&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.odroid.ballena)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.ballena&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=com.odroid.ballena)
<details>
<summary>Quality details</summary>

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.ballena&metric=bugs)](https://sonarcloud.io/dashboard?id=com.odroid.ballena)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.ballena&metric=code_smells)](https://sonarcloud.io/dashboard?id=com.odroid.ballena)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.ballena&metric=coverage)](https://sonarcloud.io/dashboard?id=com.odroid.ballena)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.ballena&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=com.odroid.ballena)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.ballena&metric=ncloc)](https://sonarcloud.io/dashboard?id=com.odroid.ballena)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.ballena&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=com.odroid.ballena)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.ballena&metric=security_rating)](https://sonarcloud.io/dashboard?id=com.odroid.ballena)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.ballena&metric=sqale_index)](https://sonarcloud.io/dashboard?id=com.odroid.ballena)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.ballena&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=com.odroid.ballena)

[Sonar analysis](https://sonarcloud.io/dashboard?id=com.odroid.ballena)
</details>

Keywords: spring, spring-boot, spring-security, blowfish, spring-data, jpa, buildinfo, gradle, lombok, retrofit, retrofit2, junit, mockmvc, mockito, Wiremock, git, sonar, sonarcloud, jacoco, flyway, hsqldb, vue, jest, docker, continuous integration, continuous quality, continuous deployment, docker, kubernetes, GKE, GCE, gitlab ci, i18n, themes, w3css, responsive, html5, mobile first, infinite-scroll, openjdk, openjdk21, MsOpenJDK, EditorConfig

(C) Javier Sedano 2013-2024.

TODO:
* e2e: find fails randomly

Instructions:

  * Clone repo
    * If you dit download it (instead of clone), GIT will not be available, so `com.gorylenko.gradle-git-properties` must be disabled in `build.gradle`.
  * Requirements:
    * Java 21 compiler. Ms OpenJDK21 (see link below) is suggested for building and testing. v21.0.1 has been tested.
    * NodeJS (version 22.12.0 is tested) with npm.
    * Firefox (needed for e2e tests). Any modern browser to use it.
      * In the current version, the e2e tests with Chrome fails. IE and Firefox work.
  * To quickly run it: `gradlew clean bootRun` and browse [ballena](http://localhost:8080/ballena/) or `gradlew applicationBrowse`.
    * Administrator: `a`, password `a`.
    * Plain user: `b`, password `b`
    * This generates DB on startup and adds some sample data.
  * To generate deliverables (remember to create database and edit application.properties if needed):
    * To generate an autorunnable jar: `gradlew bootJar`.
    * To generate a deployable war: `gradlew bootWar`.
    * To create a docker image:
      * `gradlew bootJar`.
      * VER=`cat gradle.properties | grep project_version | tr -d '\n' | tr -d '\r' | cut -d= -f2`
      * TAG="${VER}-$CI_COMMIT_SHORT_SHA"
      * `cat deploy/app/docker/Dockerfile | sed s/VERSION/$TAG/g | docker build -t ballena:$TAG -f - .`
        * Export docker image to file: `docker save ballena | gzip > ../ballena-docker.tgz`.
        * Or push it to a registry (this process is done during CI/CD and the latests iamge is pushed to [GitLab registry](https://gitlab.com/javier-sedano/ballena/container_registry)).
      * Database is migrated to latest version upon startup using flyway, but sample data is not added by default. Add `-Dspring.flyway.locations=classpath:/db/migration,classpath:/db/sample` to add sample data, if desired.
  * To study or develop it:
    * Windows is assumed in many dev tasks (not so for CI tasks); either use Windows or port them to Unix (should be pretty straightforward: look for `cmd /c`).
    * Backend (instructions for IntelliJ 2021.3, but obviously other IDEs are usable):
      * Import as gradle project into IntelliJ.
      * For former versions of IntelliJ, the Lombok plugin is needed (it is built-in by default in new versions):
        * Install plugin.
        * Enable Annotation Processing: Settings / Build, Execution, Deployment / Compiler / Annotation Processors / Enable.
        * Let IntelliJ upgrade the plugin (if offered) and restart.
      * Configure IntelliJ to use the Java version defined in the project: Settings / Build, Execution, Deployment / Build Tools / Gradle / Gradle JVM / Project SDK (or use the wrench button in the gradle panel).
      * Configure IntelliJ to "Build and run" and "Run tests" using IntelliJ (default is gradle). Do it under Settings / Build, Execution, Deployment / Build Tools / Gradle / Gradle (or use the wrench button in the gradle panel).
      * A launcher for IntelliJ is included (Ballena w/Sample): it launches backend and watch-builds frontend. Otherwise:
          * Launch in IDE with `-Dspring.config.location=src/test/resources/test.properties -Dspring.datasource.url=jdbc:hsqldb:file:build/db/app-hsql-with-sample-data/app.hsql;shutdown=true`.
            * To emulate the deployed behaviour add `-XX:+UseSerialGC -Xss512k -XX:MaxRAM=210m`.
            * This creates (or migrates) a minimal DB on startup:
              * To create (or migrate) a DB with sample data, add `-Dspring.flyway.locations=classpath:/db/migration,classpath:/db/sample`.
              * DB can aldo be created using `gradle createDB` (for minimal DB) or `createDbWithSampleData` (for demos or exploratory tests).
          * Watch-build frontend with `gradlew frontBuildWatch`
      * Launch tests in IDE or with `gradlew test`.
    * Frontend (instructions for Visual Studio Code, but obviously other IDEs are usable):
      * Import ballena-front/ directory into VisualStudioCode.
      * Add EditorConfig plugin, so .editorconfig is honored.
      * To launch tests in watch mode: `npm run frontTest`.
      * The app is served through backend (see launcher and gradle watch-build above).
    * End to end tests (instructions for Visual Studio Code, but obviously other IDEs are usable):
      * Import ballena-e2e/ directory into VisualStudioCode.
      * Add EditorConfig plugin, so .editorconfig is honored.
      * To launch tests:
        * Run the app in the backend following the instructions above and wait for it to be ready.
        * Launch `npm run dependenciesInit` and `npm run test:e2e` to test and watch the e2e tests.
    * Sonarqube is used as analysis tool:
      * A task `gradlew sonarqubeStart` is provided to run it dockerized.
      * Modern Sonar does not allow unauthenticated analysis by default. So launch Sonarqube, browse http://localhost:9000, login with `admin/admin`, you will be asked to change the password... change to `admin/admin1`.
  * Master branch is continuously deployed to [https://jsedano.duckdns.org/ballena/](https://jsedano.duckdns.org/ballena/). Notice that different passwords are used there, so you may not be able to login. Continuous deployment uses SSH keys and default passwords that are not shown in source. Notice that it is deployed in a severely limited Google Compute Engine virtual machine, so performance may not be good or may not even work.
  * Master branch is continuously deployed to [https://ballena-jsedano.duckdns.org/ballena/](https://ballena-jsedano.duckdns.org/ballena). Notice that different passwords are used there, so you may not be able to login. Continuous deployment uses SSH keys and default passwords that are not shown in source. Notice that it is deployed to a Google Kubernetes Engine cluster which is a paid-service, so I may decide to stop doing it without further notice.
  * A Windows service is downloadable from [https://jsedano.duckdns.org/ballenaWinUpdate/](https://jsedano.duckdns.org/ballenaWinUpdate/).
  * Hint for derived project:
    * Replace `ballena` to `xxx`
    * Replace `Ballena` to `Xxx`
    * Rename `ballena-front` to `xxx-front`
    * Rename `src\...\ballena\...` to `src\...\xxx\...`
    * Adapt (or remove) CD process
      * Before the first CI a default sonar analysis must be done in SonarCloud:
        * `gradlew -Psonar_base_url=https://sonarcloud.io/ sonarqube -Dsonar.organization=javiersedano -Dsonar.login=TYPE_THE_TOKEN_HERE -Dsonar.password=`
          * You will need to define "New Code" in the SonarCloud web console for the QualityGate to work.

Useful links:

* https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/
* https://docs.spring.io/spring/docs/5.1.10.RELEASE/spring-framework-reference/
* https://docs.spring.io/spring-data/jpa/docs/2.0.9.RELEASE/reference/html/
* https://flywaydb.org/
* https://www.baeldung.com/jpa-optimistic-locking
* https://learn.microsoft.com/en-us/java/openjdk/download
* https://docs.spring.io/spring-security/site/docs/5.0.12.RELEASE/reference/htmlsingle/
* https://docs.spring.io/spring-boot/docs/current/actuator-api/html/
* https://spring.io/guides/tutorials/spring-security-and-angular-js/
* https://github.com/dorongold/gradle-task-tree
* https://github.com/OSSIndex/ossindex-gradle-plugin/
* https://projectlombok.org/features/all
* https://square.github.io/retrofit/
* https://square.github.io/okhttp/
* http://wiremock.org/
* https://docs.npmjs.com/cli-documentation/
* https://www.npmjs.com/package/cross-var
* https://www.npmjs.com/package/rimraf
* https://www.w3schools.com/w3css/
* https://sass-lang.com/documentation
* https://fontawesome.com/icons?d=gallery&s=brands,regular,solid&m=free
* https://docs.gitlab.com/ee/ci/yaml/
* https://sonarcloud.io/web_api/api/qualitygates
* https://www.2uo.de/myths-about-urandom/
* https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner+for+Gradle
* https://docs.sonarqube.org/latest/analysis/analysis-parameters/
* https://kubernetes.io/docs/tutorials/
* https://cloud.google.com/kubernetes-engine/docs/
* https://kubernetes.io/docs/reference/
* https://github.com/kohsuke/winsw
* https://vuejs.org/
* https://cli.vuejs.org/
* https://router.vuejs.org/
* https://vitest.dev/
* https://lodash.com/
* https://testcafe.io/
* https://vueuse.org/core/useInfiniteScroll/
