import { t } from 'testcafe';
import notesPage from '../pages/NotesPage';

class NotesQuestions {

  async noNoteIsShow() {
    await t.expect(notesPage.none.exists).ok();
  }

  async newNoteIsShow() {
    await t
      .expect(notesPage.title.exists).ok('title not found', { timeout: 30000 })
      .expect(notesPage.content.exists).ok()
  }

  async noteIsShown(title, content) {
    await t.expect(notesPage.title.value).eql(title);
    await t.expect(notesPage.content.value).eql(content);
  }

  async noteIsFound(title) {
    await t.expect(notesPage.searchResults.count).eql(1);
    return notesPage.searchResults.child("a").withText(title);
  }

  async noNoteIsFound() {
    await t.expect(notesPage.searchResults.count).eql(0);
  }

}

export default new NotesQuestions();
