import { t } from 'testcafe';
import loginPage from '../pages/LoginPage';

class LoginQuestions {

  async loginIsShow() {
    await t.expect(loginPage.about.exists).ok();
  }

}

export default new LoginQuestions();
