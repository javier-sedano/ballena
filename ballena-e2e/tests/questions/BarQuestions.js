import { t } from 'testcafe';
import barPage from '../pages/BarPage';

class BarQuestions {

  async barIsShown() {
    await t.expect(barPage.bar.exists).ok();
  }

  async isTabShown(tabText) {
    await t.expect(barPage.selected.innerText).eql(tabText);
  }

  async notesIsShown() {
    await this.isTabShown("Notes");
  }

}

export default new BarQuestions();
