import { t } from 'testcafe';
import notesPage from '../pages/NotesPage';

class NotesTasks {

  async createNote() {
    await t.click(notesPage.add);
  }

  async modifyNote(title, content) {
    await t.click(notesPage.title)
      .pressKey("ctrl+a delete")
      .typeText(notesPage.title, title);
    await t.click(notesPage.content)
      .pressKey("ctrl+a delete")
      .typeText(notesPage.content, content);
    await t.click(notesPage.save);
  }

  async findNote(title) {
    await t.click(notesPage.search);
    await t.click(notesPage.filter)
      .pressKey("ctrl+a delete")
      .typeText(notesPage.filter, title);
    await t.wait(1001);
  }

  async clickFoundNote(noteLink) {
    await t.click(noteLink);
  }

  async deleteNote() {
    await t.click(notesPage.delete);
  }

}

export default new NotesTasks();
