import { t } from 'testcafe';
import loginPage from '../pages/LoginPage';
import barPage from '../pages/BarPage';

const adminUser = "a";
const adminPass = "a";

class LoginTasks {

  async login(user, pass) {
    await t
      .typeText(loginPage.username, user)
      .typeText(loginPage.pass, pass)
      .click(loginPage.login);
  }

  async loginAdmin() {
    await this.login(adminUser, adminPass);
  }

  async logout() {
    await t.click(barPage.menu);
    await t.click(barPage.logout);
  }

}

export default new LoginTasks();
