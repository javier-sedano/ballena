import { t } from 'testcafe';
import barPage from '../pages/BarPage';

class BarTasks {

  async goToTab(tabSelector) {
    await t.click(barPage.menu);
    await t.click(tabSelector);
  }

  async goToNotes() {
    await this.goToTab(barPage.notes);
  }

}

export default new BarTasks();
