import { Selector } from 'testcafe';

class BarPage {
  constructor() {
    this.bar = Selector("#app-bar");
    this.menu = Selector('a[aria-label="Menu"]');
    this.notes = Selector("#smallMenu a").withText("Notes");
    this.logout = Selector("#smallMenu a").withText("Logout");
    this.selected = Selector('#smallMenu a.router-link-exact-active');
  }
}

export default new BarPage();
