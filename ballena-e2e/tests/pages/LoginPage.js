import { Selector } from 'testcafe';

class LoginPage {
  constructor() {
    this.about = Selector("#login-about");
    this.username = Selector('input[name="username"]');
    this.pass = Selector('input[name="password"]');
    this.login = Selector('input[name="update"]');
  }
}

export default new LoginPage();
