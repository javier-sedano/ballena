import { Selector } from 'testcafe';

class NotesPage {
  constructor() {
    this.notesTabSelected = Selector('#app-bar-notes.router-link-exact-active');
    this.add = Selector('button[aria-label="Add"');
    this.title = Selector('input[name="title"]');
    this.content = Selector('textarea[name="content"]');
    this.save = Selector('button[aria-label="Save"');
    this.delete = Selector('button[aria-label="Delete"');
    this.search = Selector('button[aria-label="Search"');
    this.close = Selector('button[aria-label="Close"');
    this.filter = Selector('input[placeholder="Filter"]');
    this.searchResults = Selector('#notes-search-results li');
    this.none = Selector('p').withText("Choose an item from the filter");
  }
}

export default new NotesPage();
