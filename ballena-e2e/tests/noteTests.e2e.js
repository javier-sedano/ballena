import constants from './constants';
import loginQuestions from './questions/LoginQuestions';
import barQuestions from './questions/BarQuestions';
import notesQuestions from './questions/NotesQuestions';
import loginTasks from './tasks/LoginTasks';
import barTasks from './tasks/BarTasks';
import notesTasks from './tasks/NotesTasks';

fixture`Notes`
  .page`http://localhost:8080/ballena/web/`
  .beforeEach(async t => {
    await t.resizeWindowToFitDevice(constants.testDevice, { portraitOrientation: true });
  });

test('Login, create, edit, find, delete, logout', async t => {
  await loginQuestions.loginIsShow();

  await loginTasks.loginAdmin();
  await barQuestions.barIsShown();

  await barTasks.goToNotes();
  await barQuestions.notesIsShown();
  await notesQuestions.noNoteIsShow();

  await notesTasks.createNote();
  await notesQuestions.newNoteIsShow();

  const title = "T" + Math.random();
  const content = "C" + Math.random();
  await notesTasks.modifyNote(title, content);
  await notesQuestions.noteIsShown(title, content);

  await barTasks.goToNotes();
  await barQuestions.notesIsShown();

  await notesTasks.findNote(title);
  const noteLink = await notesQuestions.noteIsFound(title);

  await notesTasks.clickFoundNote(noteLink);
  await notesQuestions.noteIsShown(title, content);

  await notesTasks.deleteNote();
  await notesQuestions.noNoteIsShow();

  await notesTasks.findNote(title);
  await notesQuestions.noNoteIsFound();

  await loginTasks.logout();
  await loginQuestions.loginIsShow();
});
