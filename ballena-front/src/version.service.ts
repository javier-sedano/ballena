import axios from "axios";
import { REST_BASE_URL } from "./service.constants";

const URL = `${REST_BASE_URL}/ping/ver`;

export const versionService = {
  getVersion: async (): Promise<string> => {
    const response = await axios.get<string>(URL);
    return response.data;
  },
};
