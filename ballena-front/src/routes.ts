import { type RouteRecordRaw } from "vue-router";
import { routes as loginRoutes } from "./_children/login/routes";
import { routes as notesRoutes } from "./_children/notes/routes";
import { routes as adminRoutes } from "./_children/admin/routes";

export const routes: Readonly<RouteRecordRaw[]> = [
  {
    path: "/",
    name: "Root",
    redirect: "/notes/",
  },
  adminRoutes,
  loginRoutes,
  notesRoutes,
  {
    path: "/:pathMatch(.*)*",
    name: "NotFound",
    /* v8 ignore next 1 */
    component: () => import("./NotFound.vue"),
  },
];
