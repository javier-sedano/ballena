import i18nMessages from "./i18n.messages";

export const i18nConfig = {
  legacy: false,
  locale: "en",
  fallbackLocale: "en",
  messages: i18nMessages,
};
