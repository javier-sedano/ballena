import { createI18n } from "vue-i18n";
import { i18nConfig } from "./i18n.config";

export const i18n = createI18n(i18nConfig);

export type I18nType = typeof i18n;
