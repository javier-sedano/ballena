import en from './en/i18n';
import es from './es/i18n';

export default {
  en,
  es,
}
