export default {
  username: "Username",
  password: "Password", // NOSONAR
  password2: "Password confirmation", // NOSONAR
  passwordMissmatch: "(Do not match)", // NOSONAR
  enabled: "Enabled",
  name: "Name",
  admin: "Is admin",
  errorI18nCode: {
    usernameCollision: "Username already in use",
  },
}
