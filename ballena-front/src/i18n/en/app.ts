export default {
  notesTab: "Notes",
  adminTab: "Admin",
  logout: "Logout {username}",
}
