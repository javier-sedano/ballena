export default {
  filter: "Filter",
  empty: "Choose an item from the filter on the left or create a new one with the icon",
  notFound: "Not found",
  notFoundMessage: "Ups! Page not found. Maybe you are using an old link. If you are sure your link is correct, please report a bug.",
  errorI18nCode: {
    forbidden: "Forbbiden",
    notFound: "{entity} {id} not found",
    concurrency: "Concurrently modified by another person",
    linkedData: "Unable to delete, there is data linked to this entity",
  },
}
