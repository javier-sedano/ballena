import system from './admin/system';
import users from './admin/users';

export default {
  systemTab: "System",
  usersTab: "Users",
  system,
  users,
}
