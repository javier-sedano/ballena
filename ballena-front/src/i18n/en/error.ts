export default {
  i18nCode: {
    default: "Unexpected error: {message}",
  },
  title: "Error",
  ok: "Ok",
}
