export default {
  i18nCode: {
    default: "Error inexperado: {message}",
  },
  title: "Error",
  ok: "Ok",
}
