export default {
  filter: "Filtro",
  empty: "Elige un elemento del filtro a la izquierda o crea uno nuevo con el icono",
  notFound: "No encontrado",
  notFoundMessage: "¡Ups! Página no encontrada. Quizá estás usando un enlace antiguo. Si estás seguro de que tu enlace es correcto, por favor, informa de un error.",
  errorI18nCode: {
    forbidden: "Prohibido",
    notFound: "No encontrado: {entity} {id}",
    concurrency: "Modificado concurrentemente por otra persona",
    linkedData: "Imposible borrar, hay datos enlazados a esta entidad",
  },
}
