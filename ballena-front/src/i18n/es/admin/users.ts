export default {
  username: "Usuario",
  password: "Contraseña", // NOSONAR
  password2: "Confirmación de password", // NOSONAR
  passwordMissmatch: "(No coinciden)", // NOSONAR
  enabled: "Habilitado",
  name: "Nombre",
  admin: "Es administrador",
  errorI18nCode: {
    usernameCollision: "El nombre de usuario ya está en uso",
  },
}
