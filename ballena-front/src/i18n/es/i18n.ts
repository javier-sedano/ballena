import common from './common';
import app from './app';
import error from './error';
import login from './login';
import admin from './admin';
import notes from './notes';

export default {
  common,
  error,
  app,
  login,
  admin,
  notes,
}
