import system from './admin/system';
import users from './admin/users';

export default {
  systemTab: "Sistema",
  usersTab: "Usuarios",
  system,
  users,
}
