import { createApp } from "vue";
import { createPinia } from "pinia";
import { FontAwesomeIcon, FontAwesomeLayers } from "@fortawesome/vue-fontawesome";
import App from "./App.vue";
import { router } from "./router";
import { setupErrorHandler } from "./error/error.handler";
import { i18n } from "./i18n/i18n";

const app = createApp(App)
  .component("font-awesome-icon", FontAwesomeIcon)
  .component("font-awesome-layers", FontAwesomeLayers)
  .use(i18n)
  .use(createPinia())
  .use(router);

app.config.errorHandler = setupErrorHandler(router, i18n);

app.mount("#app");
