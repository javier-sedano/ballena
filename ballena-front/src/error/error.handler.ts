import type { ComponentPublicInstance } from "vue";
import { type Router } from "vue-router";
import { type I18nType } from "../i18n/i18n";
import { useErrorStore } from "./error.store";
import { useAuthenticationStore } from "../_children/login/authentication.store";

const DEFAULT_ERROR_MESSAGE = "Unknown error";
const DEFAULT_I18N_CODE = "error.i18nCode.default";
const DEFAULT_I18N_ARGS = "Unknown";

export function setupErrorHandler(
  newRouter: Router,
  newI18n: I18nType,
): (err: unknown, _instance: ComponentPublicInstance | null, _info: string) => void {
  const router = newRouter;
  const i18n = newI18n;

  function isServerAuthError(err: any): boolean {
    return err?.status === 401;
  }

  function handleControlledErrors(err: any): void {
    let errorMessage = DEFAULT_ERROR_MESSAGE;
    let i18nCode = DEFAULT_I18N_CODE;
    let i18nArgs = { message: DEFAULT_I18N_ARGS };

    try {
      errorMessage = err.message; // NOSONAR
      try {
        i18nArgs = { message: errorMessage }; // NOSONAR
        i18nArgs = err.response.data.args;
        i18nCode = err.response.data.i18nCode;
        console.warn("Controlled error: ", i18nCode, i18nArgs);
      } finally {
        errorMessage = i18n.global.t(i18nCode, i18nArgs);
      }
    } catch (e) {
      console.error(e);
    } finally {
      useErrorStore().setErrorMessage(errorMessage);
    }
  }

  function errorHandler(err: unknown, _instance: ComponentPublicInstance | null, _info: string) {
    const authenticationStore = useAuthenticationStore();
    console.error(err);
    if (isServerAuthError(err)) {
      authenticationStore.logout();
      router.push({ name: "Login" });
      return;
    }
    handleControlledErrors(err);
  }
  return errorHandler;
}
