import { type RouteRecordRaw } from "vue-router";

export const routes: RouteRecordRaw = {
  path: "system",
  name: "AdminSystem",
  /* v8 ignore next 1 */
  component: () => import("./System.vue"),
};
