import { describe, it, expect, afterEach, vi } from "vitest";
import { mount, VueWrapper } from "@vue/test-utils";
import { mockI18n } from "../../../../../../__tests__/test.utils";
import System from "../../System.vue";

mockI18n();

afterEach(() => {
  vi.clearAllMocks();
});

describe("System screen", async () => {
  function createWrapper(): VueWrapper {
    const wrapper = mount(System);
    return wrapper;
  }
  it("Should render", async () => {
    const wrapper = createWrapper();
    expect(wrapper.element).toMatchSnapshot("Initial");
  });
});
