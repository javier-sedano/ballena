import { describe, it, expect, afterEach, vi } from "vitest";
import { mount, flushPromises, VueWrapper } from "@vue/test-utils";
import { type RouteLocationNormalizedLoadedGeneric } from "vue-router";
import { mockI18n, mockAxios, BASE_REST_URL, mockRouter } from "../../../../../../__tests__/test.utils";
import Users from "../../Users.vue";
import { type MinimalUser } from "../../../users/users.service";

mockI18n();
const { useRouteMock, mockRouterPush } = mockRouter();
const routerPushMock = mockRouterPush();
const { axiosGetMock, axiosPostMock } = mockAxios();

afterEach(() => {
  vi.clearAllMocks();
});

describe("Users screen", async () => {
  const MINIMAL_USER1: MinimalUser = {
    id: 1,
    username: "username1",
  };
  const MINIMAL_USER2: MinimalUser = {
    id: 2,
    username: "username2",
  };
  const MINIMAL_USERS: MinimalUser[] = [
    MINIMAL_USER1,
    MINIMAL_USER2,
  ];
  const USERS_REST_URL = `${BASE_REST_URL}/users`;

  async function createWrapper(): Promise<VueWrapper> {
    useRouteMock.mockReturnValue({ fullPath: "/users/" } as RouteLocationNormalizedLoadedGeneric);
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: MINIMAL_USERS }));
    const wrapper = mount(Users, {
      global: {
        stubs: {
          "router-link": { template: "<span router-link><slot/></span>" },
          "router-view": { template: "<span router-view/>" },
          "font-awesome-icon": { template: "<span font-awesome-icon/>" },
        },
      },
      attachTo: document.body,
    });
    expect((wrapper.vm as any).isFinding).toEqual(true);
    expect(wrapper.element).toMatchSnapshot("Initial");
    await flushPromises();
    expect(axiosGetMock).toHaveBeenCalledWith(USERS_REST_URL, { params: { filter: "", from: 0 } });
    axiosGetMock.mockClear();
    expect((wrapper.vm as any).isFinding).toEqual(false);
    expect((wrapper.vm as any).users).toEqual(MINIMAL_USERS);
    expect(wrapper.element).toMatchSnapshot("Loaded");
    return wrapper;
  }
  it("Should render", async () => {
    await createWrapper();
  });
  async function testFilter(action: (wrapper: VueWrapper) => Promise<void>): Promise<VueWrapper> {
    const FILTER = "1";
    const wrapper = await createWrapper();
    axiosGetMock.mockImplementationOnce(() =>
      Promise.resolve({
        data: [
          MINIMAL_USER1,
        ],
      }),
    );
    wrapper.find('[data-test-id~="filterField"]').setValue(FILTER);
    await action(wrapper);
    await flushPromises();
    expect(axiosGetMock).toHaveBeenCalledWith(USERS_REST_URL, { params: { filter: FILTER, from: 0 } });
    axiosGetMock.mockClear();
    expect((wrapper.vm as any).users).toEqual([
      MINIMAL_USER1,
    ]);
    return wrapper;
  }
  it("Should filter users", async () => {
    const wrapper = await testFilter(async (wrapper) => {
      await wrapper.find('[data-test-id~="filterField"]').trigger("keyup");
    });
    expect(wrapper.element).toMatchSnapshot("Filtered");
  });
  it("Should refresh search", async () => {
    const wrapper = await testFilter(async (wrapper) => {
      await wrapper.find('[data-test-id~="findButton"]').trigger("click");
    });
    expect(wrapper.element).toMatchSnapshot("Filtered");
  });
  it("Should create a new user", async () => {
    const MINIMAL_USER3: MinimalUser = {
      id: 3,
      username: "username3",
    };
    const NEW_MINIMAL_USERS: MinimalUser[] = [
      ...MINIMAL_USERS,
      MINIMAL_USER3,
    ];
    const wrapper = await createWrapper();
    axiosPostMock.mockImplementationOnce(() => Promise.resolve({ data: MINIMAL_USER3.id }));
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: NEW_MINIMAL_USERS }));
    expect((wrapper.vm as any).isAdding).toEqual(false);
    await wrapper.find('[data-test-id~="addButton"]').trigger("click");
    expect((wrapper.vm as any).isAdding).toEqual(true);
    await flushPromises();
    expect((wrapper.vm as any).isAdding).toEqual(false);
    expect(axiosPostMock).toHaveBeenCalledWith(USERS_REST_URL);
    expect(axiosGetMock).toHaveBeenCalledWith(USERS_REST_URL, { params: { filter: "", from: 0 } });
    expect((wrapper.vm as any).users).toEqual(NEW_MINIMAL_USERS);
    expect(routerPushMock).toHaveBeenCalledWith({ name: "AdminUserDetails", params: { id: MINIMAL_USER3.id } });
    expect(wrapper.element).toMatchSnapshot("Created");
  });
  it("Should select a user", async () => {
    const SELECTED_USER_ID = 1;
    const wrapper = await createWrapper();
    await (wrapper.vm as any).selectUser(SELECTED_USER_ID);
    flushPromises();
    expect((wrapper.vm as any).selectedUserId).toEqual(SELECTED_USER_ID);
    expect((wrapper.vm as any).showSmallSearchPanel).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("User selected");
  });
  it("Should show and hide the search panel", async () => {
    const wrapper = await createWrapper();
    expect(wrapper.find('[data-test-id~="smallSearchPanel"]').isVisible()).toEqual(false);
    await wrapper.find('[data-test-id~="showSmallSearchButton"]').trigger("click");
    expect(wrapper.find('[data-test-id~="smallSearchPanel"]').isVisible()).toEqual(true);
    await wrapper.find('[data-test-id~="hideSmallSearchButton"]').trigger("click");
    expect(wrapper.find('[data-test-id~="smallSearchPanel"]').isVisible()).toEqual(false);
  });
});
