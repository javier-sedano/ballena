import { describe, it, expect, afterEach, vi } from "vitest";
import { mount, flushPromises, VueWrapper } from "@vue/test-utils";
import { type RouteLocationNormalizedLoadedGeneric } from "vue-router";
import { mockI18n, mockAxios, BASE_REST_URL, mockRouter, SHOWN_EVENT } from "../../../../../../__tests__/test.utils";
import { type DetailedUser } from "../../users.service";
import UserDetails from "../../UserDetails.vue";

mockI18n();
const { useRouteMock, mockRouterPush } = mockRouter();
const routerPushMock = mockRouterPush();
const { axiosGetMock, axiosPutMock, axiosDeleteMock } = mockAxios();

afterEach(() => {
  vi.clearAllMocks();
});

describe("User details screen", async () => {
  const USER: DetailedUser = {
    id: 1,
    username: "username1",
    name: "Name1",
    hash: "$2y$10$CGJXdWNwIIni2Ps7Mvhb7O.Zh2Fi9iiHg5v8ud643CWuKqPpH0KYm",
    enabled: false,
    admin: false,
    version: 1,
  };
  const USERS_REST_URL = `${BASE_REST_URL}/users`;
  const UPDATED_EVENT = "updated";

  async function createWrapper(): Promise<VueWrapper> {
    useRouteMock.mockReturnValue({ params: { id: USER.id } } as unknown as RouteLocationNormalizedLoadedGeneric);
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: USER }));
    const wrapper = mount(UserDetails, {
      global: {
        stubs: {
          "font-awesome-icon": { template: "<span font-awesome-icon/>" },
        },
      },
    });
    expect(wrapper.element).toMatchSnapshot("Initial");
    await flushPromises();
    expect(axiosGetMock).toHaveBeenCalledWith(`${USERS_REST_URL}/${USER.id}`);
    axiosGetMock.mockClear();
    expect((wrapper.vm as any).user).toEqual({ ...USER, hashConfirmation: USER.hash });
    expect(wrapper.emitted()).toHaveProperty(SHOWN_EVENT);
    expect(wrapper.element).toMatchSnapshot("Loaded");
    return wrapper;
  }

  it("Should render", async () => {
    await createWrapper();
  });

  const MODIFIED_USER: DetailedUser = {
    id: 1,
    username: "savedUsername1",
    name: "SavedName1",
    hash: "qw3rty",
    enabled: true,
    admin: true,
    version: 1,
  };

  it("Should save a note", async () => {
    const SAVED_USER: DetailedUser = {
      ...MODIFIED_USER,
      hash: "$2y$10$W3JK.fibwIoYGyLIuZcp7.R0.ZxwTj568y8ua08DKdZJyeYxUoccO",
      version: 2,
    };
    const wrapper = await createWrapper();
    wrapper.find('[data-test-id~="usernameField"]').setValue(MODIFIED_USER.username);
    wrapper.find('[data-test-id~="passwordField"]').setValue(MODIFIED_USER.hash);
    wrapper.find('[data-test-id~="passwordConfirmatonField"]').setValue(MODIFIED_USER.hash);
    wrapper.find('[data-test-id~="enabledField"]').setValue(MODIFIED_USER.enabled);
    wrapper.find('[data-test-id~="nameField"]').setValue(MODIFIED_USER.name);
    wrapper.find('[data-test-id~="adminField"]').setValue(MODIFIED_USER.admin);
    expect((wrapper.vm as any).isSaving).toEqual(false);
    axiosPutMock.mockImplementationOnce(() => Promise.resolve({ data: SAVED_USER }));
    wrapper.find('[data-test-id~="saveButton"]').trigger("click");
    expect(axiosPutMock).toHaveBeenCalledWith(USERS_REST_URL, MODIFIED_USER);
    expect((wrapper.vm as any).isSaving).toEqual(true);
    await flushPromises();
    expect((wrapper.vm as any).isSaving).toEqual(false);
    expect((wrapper.vm as any).user).toEqual({ ...SAVED_USER, hashConfirmation: SAVED_USER.hash });
    expect(wrapper.emitted()).toHaveProperty(UPDATED_EVENT);
    expect(wrapper.element).toMatchSnapshot("Saved");
    return wrapper;
  });

  it("Should detect fail to save a user", async () => {
    const ERROR_SAVING = "Failed to save";
    const wrapper = await createWrapper();
    wrapper.find('[data-test-id~="usernameField"]').setValue(MODIFIED_USER.username);
    wrapper.find('[data-test-id~="passwordField"]').setValue(MODIFIED_USER.hash);
    wrapper.find('[data-test-id~="passwordConfirmatonField"]').setValue(MODIFIED_USER.hash);
    wrapper.find('[data-test-id~="enabledField"]').setValue(MODIFIED_USER.enabled);
    wrapper.find('[data-test-id~="nameField"]').setValue(MODIFIED_USER.name);
    wrapper.find('[data-test-id~="adminField"]').setValue(MODIFIED_USER.admin);
    expect((wrapper.vm as any).isSaving).toEqual(false);
    axiosPutMock.mockImplementationOnce(() => Promise.reject(new Error(ERROR_SAVING)));
    await expect(() => (wrapper.vm as any).save()).rejects.toThrowError(ERROR_SAVING);
    expect(axiosPutMock).toHaveBeenCalledWith(USERS_REST_URL, MODIFIED_USER);
    expect((wrapper.vm as any).isSaving).toEqual(false);
    expect((wrapper.vm as any).user).toEqual({ ...MODIFIED_USER, hashConfirmation: MODIFIED_USER.hash });
    expect(wrapper.emitted()).not.toHaveProperty(UPDATED_EVENT);
    expect(wrapper.element).toMatchSnapshot("Not saved");
  });

  it("Should disable if passwords do not match", async () => {
    const wrapper = await createWrapper();
    expect(wrapper.find('[data-test-id~="saveButton"]').attributes().disabled).toEqual(undefined);
    wrapper.find('[data-test-id~="usernameField"]').setValue(MODIFIED_USER.username);
    wrapper.find('[data-test-id~="passwordField"]').setValue(MODIFIED_USER.hash);
    wrapper.find('[data-test-id~="passwordConfirmatonField"]').setValue("Wrong");
    wrapper.find('[data-test-id~="enabledField"]').setValue(MODIFIED_USER.enabled);
    wrapper.find('[data-test-id~="nameField"]').setValue(MODIFIED_USER.name);
    wrapper.find('[data-test-id~="adminField"]').setValue(MODIFIED_USER.admin);
    await flushPromises();
    expect(wrapper.find('[data-test-id~="saveButton"]').attributes().disabled).toEqual("");

    expect(wrapper.element).toMatchSnapshot("Unmatching passwords");
  });

  it("Should delete a user", async () => {
    const wrapper = await createWrapper();
    expect((wrapper.vm as any).isDeleting).toEqual(false);
    axiosDeleteMock.mockImplementationOnce(() => Promise.resolve());
    wrapper.find('[data-test-id~="deleteButton"]').trigger("click");
    expect(axiosDeleteMock).toHaveBeenCalledWith(`${USERS_REST_URL}/${USER.id}`);
    expect((wrapper.vm as any).isDeleting).toEqual(true);
    await flushPromises();
    expect((wrapper.vm as any).isDeleting).toEqual(false);
    expect(wrapper.emitted()).toHaveProperty(UPDATED_EVENT);
    expect(routerPushMock).toHaveBeenCalledWith({ name: "AdminUserNone" });
    expect(wrapper.element).toMatchSnapshot("Deleted");
    return wrapper;
  });

  it("Should detect fail to delete a user", async () => {
    const ERROR_DELETING = "Failed to delete";
    const wrapper = await createWrapper();
    expect((wrapper.vm as any).isDeleting).toEqual(false);
    axiosDeleteMock.mockImplementationOnce(() => Promise.reject(new Error(ERROR_DELETING)));
    await expect(() => (wrapper.vm as any).deletee()).rejects.toThrowError(ERROR_DELETING);
    expect(axiosDeleteMock).toHaveBeenCalledWith(`${USERS_REST_URL}/${USER.id}`);
    expect((wrapper.vm as any).isDeleting).toEqual(false);
    expect(wrapper.emitted()).not.toHaveProperty(UPDATED_EVENT);
    expect(wrapper.element).toMatchSnapshot("Not deleted");
    return wrapper;
  });
});
