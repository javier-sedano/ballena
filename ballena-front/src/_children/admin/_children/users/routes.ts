import { type RouteRecordRaw } from "vue-router";

export const routes: RouteRecordRaw = {
  path: "users/",
  name: "AdminUsers",
  /* v8 ignore next 1 */
  component: () => import("./Users.vue"),
  children: [
    {
      path: "",
      name: "AdminUserNone",
      /* v8 ignore next 1 */
      component: () => import("../../../../NoneDetails.vue"),
    },
    {
      path: ":id",
      name: "AdminUserDetails",
      /* v8 ignore next 1 */
      component: () => import("./UserDetails.vue"),
    },
  ],
};
