import { type DetailedUser } from "./users.service";

export class DetailedUserViewModel {
  id: number;
  username: string;
  hash: string;
  hashConfirmation: string;
  enabled: boolean;
  name: string;
  version: number;
  admin: boolean;

  constructor(detailedUser: DetailedUser) {
    this.id = detailedUser.id;
    this.username = detailedUser.username;
    this.hash = detailedUser.hash;
    this.hashConfirmation = detailedUser.hash;
    this.enabled = detailedUser.enabled;
    this.name = detailedUser.name;
    this.version = detailedUser.version;
    this.admin = detailedUser.admin;
  }

  toDetailedUser(): DetailedUser {
    return {
      id: this.id,
      username: this.username,
      hash: this.hash,
      enabled: this.enabled,
      name: this.name,
      version: this.version,
      admin: this.admin,
    };
  }

  passwordsMatch(): boolean {
    return this.hash === this.hashConfirmation;
  }
}
