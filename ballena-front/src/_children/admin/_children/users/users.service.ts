import axios from "axios";
import { REST_BASE_URL } from "../../../../service.constants";

const URL = `${REST_BASE_URL}/users`;

export interface MinimalUser {
  id: number;
  username: string;
}

export interface DetailedUser {
  id: number;
  username: string;
  hash: string;
  enabled: boolean;
  name: string;
  admin: boolean;
  version: number;
}

export const usersService = {
  findUsers: async (filter: string, from: number): Promise<MinimalUser[]> => {
    const response = await axios.get<MinimalUser[]>(URL, { params: { filter, from } });
    return response.data;
  },
  getUser: async (id: number): Promise<DetailedUser> => {
    const response = await axios.get<DetailedUser>(`${URL}/${id}`);
    return response.data;
  },
  updateUser: async (user: DetailedUser): Promise<DetailedUser> => {
    const response = await axios.put<DetailedUser>(URL, user);
    return response.data;
  },
  deleteUser: async (id: number): Promise<void> => {
    await axios.delete<void>(`${URL}/${id}`);
  },
  createUser: async (): Promise<number> => {
    const response = await axios.post<number>(URL);
    return response.data;
  },
};
