import { type RouteRecordRaw } from "vue-router";
import { routes as systemRoutes } from "./_children/system/routes";
import { routes as usersRoutes } from "./_children/users/routes";
import { isAdminGuard } from "../../_children/login/guards";

export const routes: RouteRecordRaw = {
  path: "/admin/",
  name: "Admin",
  /* v8 ignore next 1 */
  component: () => import("./Admin.vue"),
  children: [
    {
      name: "AdminRoot",
      path: "",
      redirect: "/admin/system",
    },
    systemRoutes,
    usersRoutes,
  ],
  beforeEnter: isAdminGuard,
};
