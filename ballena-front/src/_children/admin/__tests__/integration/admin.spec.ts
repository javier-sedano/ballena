import { describe, it, expect, afterEach, vi } from "vitest";
import { mount, VueWrapper } from "@vue/test-utils";
import { mockI18n } from "../../../../__tests__/test.utils";
import Admin from "../../Admin.vue";

mockI18n();

afterEach(() => {
  vi.clearAllMocks();
});

describe("Admin container", async () => {
  function createWrapper(): VueWrapper {
    const wrapper = mount(Admin, {
      global: {
        stubs: {
          "router-link": { template: "<span router-link><slot/></span>" },
          "router-view": { template: "<span router-view/>" },
          "font-awesome-icon": { template: "<span font-awesome-icon/>" },
        },
      },
      attachTo: document.body,
    });
    return wrapper;
  }
  it("Should render", async () => {
    const wrapper = createWrapper();
    expect(wrapper.element).toMatchSnapshot("Initial");
  });
  it("Should show and hide the menu", async () => {
    const wrapper = await createWrapper();
    const burgerButton = wrapper.find('[data-test-id~="burgerButton"]');
    const smallMenu = wrapper.find('[data-test-id~="smallMenu"]');
    expect(smallMenu.isVisible()).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("Before toggle menu");
    await burgerButton.trigger("click");
    expect(smallMenu.isVisible()).toEqual(true);
    expect(wrapper.element).toMatchSnapshot("After show menu");
    await smallMenu.trigger("click");
    expect(smallMenu.isVisible()).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("After hide menu");
    await burgerButton.trigger("click");
    expect(smallMenu.isVisible()).toEqual(true);
    expect(wrapper.element).toMatchSnapshot("After show menu");
    await burgerButton.trigger("click");
    expect(smallMenu.isVisible()).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("After hide menu");
  });
});
