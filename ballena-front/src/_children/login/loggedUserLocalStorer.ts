import { type User } from "./User";

export interface VersionedUser extends User {
  version: number;
}

const APP_LOGGEDUSER_LOCALSTORAGE_KEY = "ballenaLoggedUser";
const LOGGED_USER_VERSION = 1;

export function getStoredLoggedUser(): User | null {
  const loggedUserJson = localStorage.getItem(APP_LOGGEDUSER_LOCALSTORAGE_KEY);
  if (loggedUserJson === null) {
    return null;
  }
  try {
    const loggedUser: VersionedUser = JSON.parse(loggedUserJson);
    if (loggedUser.version === undefined || loggedUser.version !== LOGGED_USER_VERSION) {
      console.warn("Discarding invalid loggedUser data: ", loggedUser);
      return null;
    }
    return loggedUser;
  } catch (e) {
    console.error(e);
    console.warn("Discarding invalid loggedUser data: ", loggedUserJson);
    return null;
  }
}

export function storeLoggedUser(user: User): User {
  const loggedUser: VersionedUser = {
    ...user,
    version: LOGGED_USER_VERSION,
  };
  const loggedUserJson: string = JSON.stringify(loggedUser);
  console.debug("Storing logged user", loggedUser);
  localStorage.setItem(APP_LOGGEDUSER_LOCALSTORAGE_KEY, loggedUserJson);
  return loggedUser;
}

export function clearStoredLoggedUser() {
  localStorage.removeItem(APP_LOGGEDUSER_LOCALSTORAGE_KEY);
}
