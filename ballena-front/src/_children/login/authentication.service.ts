import axios from "axios";
import { REST_BASE_URL } from "../../service.constants";
import { type User } from "./User";

const LOGIN_URL = `${REST_BASE_URL}/login`;
const LOGOUT_URL = `${REST_BASE_URL}/logout`;

export const authenticationService = {
  login: async (username: string, password: string): Promise<User> => {
    const loginParams = new FormData();
    loginParams.append("username", username);
    loginParams.append("password", password);
    const response = await axios.post<User>(LOGIN_URL, loginParams);
    return response.data;
  },
  logout: async (): Promise<void> => {
    await axios.post<void>(LOGOUT_URL);
  },
};
