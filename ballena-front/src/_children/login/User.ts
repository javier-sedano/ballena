export const ROLE_ADMIN = "ROLE_ADMIN";
export const ROLE_USER = "ROLE_USER";
export type Role = typeof ROLE_ADMIN | typeof ROLE_USER;
export interface User {
  username: string;
  roles: Role[];
}
