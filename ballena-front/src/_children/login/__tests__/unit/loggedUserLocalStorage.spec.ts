import { describe, it, expect } from "vitest";
import {
  getStoredLoggedUser,
  storeLoggedUser,
  clearStoredLoggedUser,
  type VersionedUser,
} from "../../loggedUserLocalStorer";
import { type User } from "../../User";
import { ADMIN_USER, APP_LOGGEDUSER_LOCALSTORAGE_KEY } from "../../../../__tests__/test.utils";

describe("loggedUserLocalStorer", () => {
  it("Should start empty", () => {
    localStorage.removeItem(APP_LOGGEDUSER_LOCALSTORAGE_KEY);
    expect(getStoredLoggedUser()).toBeNull();
  });
  it("Should store a correct user", () => {
    storeLoggedUser(ADMIN_USER);
    expect(getStoredLoggedUser()).toEqual(expect.objectContaining(ADMIN_USER));
  });
  it("Should clear the stored user", () => {
    storeLoggedUser(ADMIN_USER);
    clearStoredLoggedUser();
    expect(getStoredLoggedUser()).toBeNull();
  });
  it("Should discard unversioned stored users", () => {
    const NO_VERSIONED_USER: User = {
      ...ADMIN_USER,
    };
    const WRONG_VERSION_USER_JSON = JSON.stringify(NO_VERSIONED_USER);
    localStorage.setItem(APP_LOGGEDUSER_LOCALSTORAGE_KEY, WRONG_VERSION_USER_JSON);
    expect(getStoredLoggedUser()).toBeNull();
  });
  it("Should discard incorrect-version stored users", () => {
    const WRONG_VERSION_USER: VersionedUser = {
      ...ADMIN_USER,
      version: 0,
    };
    const WRONG_VERSION_USER_JSON = JSON.stringify(WRONG_VERSION_USER);
    localStorage.setItem(APP_LOGGEDUSER_LOCALSTORAGE_KEY, WRONG_VERSION_USER_JSON);
    expect(getStoredLoggedUser()).toBeNull();
  });
  it("Should discard incorrectly stored users", () => {
    const VERSIONED_USER: VersionedUser = {
      ...ADMIN_USER,
      version: 1,
    };
    const WRONG_USER_JSON = JSON.stringify(VERSIONED_USER).replace("}", "{");
    localStorage.setItem(APP_LOGGEDUSER_LOCALSTORAGE_KEY, WRONG_USER_JSON);
    expect(getStoredLoggedUser()).toBeNull();
  });
});
