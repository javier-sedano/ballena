import { describe, it, expect, beforeEach, afterEach, vi } from "vitest";
import { setActivePinia, createPinia } from "pinia";
import {
  mockAxios,
  USER_USER,
  USER_USERNAME,
  USER_PASSWORD,
  ADMIN_USER,
  ADMIN_USERNAME,
  ADMIN_PASSWORD,
  VERSION,
} from "../../../../__tests__/test.utils";
import { useAuthenticationStore } from "../../../login/authentication.store";
import { isAdminGuard, isUserGuard } from "../../guards";
import { type RouteLocationNormalized } from "vue-router";
import { type User } from "../../User";

const { axiosPostMock, axiosGetMock } = mockAxios();

beforeEach(() => {
  setActivePinia(createPinia());
});

afterEach(() => {
  vi.clearAllMocks();
});

describe("Guards", async () => {
  const SOME_TO: RouteLocationNormalized = {
    path: "/some/url",
  } as RouteLocationNormalized;
  const SOME_FROM: RouteLocationNormalized = {
    path: "/another/url",
  } as RouteLocationNormalized;
  const LOGIN_URL = "/login";
  async function login(username: string, password: string, user: User): Promise<void> {
    const authenticationStore = useAuthenticationStore();
    axiosPostMock.mockImplementationOnce(() => Promise.resolve({ data: user }));
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: VERSION }));
    await authenticationStore.login(username, password);
    axiosPostMock.mockClear();
    axiosGetMock.mockClear();
  }
  it("Should allow admin check for admin user", async () => {
    await login(ADMIN_USERNAME, ADMIN_PASSWORD, ADMIN_USER);
    expect(isAdminGuard(SOME_TO, SOME_FROM)).toBeUndefined();
  });
  it("Should disallow admin check for plain user", async () => {
    await login(USER_USERNAME, USER_PASSWORD, USER_USER);
    expect(isAdminGuard(SOME_TO, SOME_FROM)).toEqual(LOGIN_URL);
    expect(useAuthenticationStore().getRedirectUrl()).toEqual(SOME_TO.path);
  });
  it("Should allow user check for plain user", async () => {
    await login(USER_USERNAME, USER_PASSWORD, USER_USER);
    expect(isUserGuard(SOME_TO, SOME_FROM)).toBeUndefined();
  });
  it("Should disallow admin check for plain user", async () => {
    const DUMMY_USERNAME = "User";
    const DUMMY_PASSWORD = "U53r";
    const DUMMY_USER: User = {
      username: DUMMY_USERNAME,
      roles: [],
    };
    await login(DUMMY_USERNAME, DUMMY_PASSWORD, DUMMY_USER);
    expect(isAdminGuard(SOME_TO, SOME_FROM)).toEqual(LOGIN_URL);
    expect(useAuthenticationStore().getRedirectUrl()).toEqual(SOME_TO.path);
  });
});
