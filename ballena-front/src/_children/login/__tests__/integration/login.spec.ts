import { describe, it, expect, beforeEach, afterEach, vi } from "vitest";
import { mount, flushPromises, VueWrapper } from "@vue/test-utils";
import { setActivePinia, createPinia } from "pinia";
import {
  mockI18n,
  mockAxios,
  mockRouter,
  BASE_REST_URL,
  ADMIN_USERNAME,
  ADMIN_PASSWORD,
  ADMIN_USER,
  ADMIN_FORMDATA,
  VERSION,
} from "../../../../__tests__/test.utils";
import Login from "../../Login.vue";
import { useAuthenticationStore } from "../../authentication.store";

mockI18n();
const { mockRouterPush } = mockRouter();
const routerPushMock = mockRouterPush();
const { axiosGetMock, axiosPostMock } = mockAxios();

beforeEach(() => {
  setActivePinia(createPinia());
});

afterEach(() => {
  vi.clearAllMocks();
});

describe("Login screen", async () => {
  const DEFAULT_REDIRECT_URL = "/";
  const SOME_REDIRECT_URL = "/some/redirect/url";

  async function createWrapper(): Promise<VueWrapper> {
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: VERSION }));
    const wrapper = mount(Login, {
      global: {
        stubs: [
          "font-awesome-icon",
        ],
      },
    });
    expect(wrapper.element).toMatchSnapshot("Initial");
    await flushPromises();
    expect(axiosGetMock).toHaveBeenCalledWith(`${BASE_REST_URL}/ping/ver`);
    axiosGetMock.mockClear();
    const authenticationStore = useAuthenticationStore();
    expect(authenticationStore.loggedUser).toBeNull();
    return wrapper;
  }
  async function createWrapperIncomming(): Promise<VueWrapper> {
    const authenticationStore = useAuthenticationStore();
    expect(authenticationStore.getRedirectUrl()).toEqual(DEFAULT_REDIRECT_URL);
    authenticationStore.setRedirectUrl(SOME_REDIRECT_URL);
    expect(authenticationStore.getRedirectUrl()).toEqual(SOME_REDIRECT_URL);
    const wrapper = await createWrapper();
    return wrapper;
  }
  it("Should render", async () => {
    const wrapper = await createWrapper();
    expect(wrapper.element).toMatchSnapshot("Mounted");
  });
  it("Should login", async () => {
    const wrapper = await createWrapperIncomming();
    axiosPostMock.mockImplementationOnce(() => Promise.resolve({ data: ADMIN_USER }));
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: VERSION }));
    wrapper.find('[data-test-id~="usernameField"]').setValue(ADMIN_USERNAME);
    wrapper.find('[data-test-id~="passwordField"]').setValue(ADMIN_PASSWORD);
    await wrapper.find('[data-test-id~="loginForm"]').trigger("submit");
    await flushPromises();
    expect(axiosPostMock).toHaveBeenCalledWith(`${BASE_REST_URL}/login`, ADMIN_FORMDATA);
    expect(axiosGetMock).toHaveBeenCalledWith(`${BASE_REST_URL}/ping/ver`);
    expect((wrapper.vm as any).failed).toEqual(false);
    expect((wrapper.vm as any).isLogining).toEqual(false);
    const authenticationStore = useAuthenticationStore();
    expect(authenticationStore.getRedirectUrl()).toEqual(DEFAULT_REDIRECT_URL);
    expect(routerPushMock).toHaveBeenCalledWith(SOME_REDIRECT_URL);
  });
  it("Should detect failed login", async () => {
    const WRONG_ADMIN_PASSWORD = `wrong-${ADMIN_PASSWORD}`;
    const WRONG_ADMIN_FORMDATA = new FormData();
    WRONG_ADMIN_FORMDATA.append("username", ADMIN_USERNAME);
    WRONG_ADMIN_FORMDATA.append("password", WRONG_ADMIN_PASSWORD);
    const AN_ERROR = "An error";
    const wrapper = await createWrapperIncomming();
    axiosPostMock.mockImplementationOnce(() => Promise.reject(new Error(AN_ERROR)));
    wrapper.find('[data-test-id~="usernameField"]').setValue(ADMIN_USERNAME);
    wrapper.find('[data-test-id~="passwordField"]').setValue(WRONG_ADMIN_PASSWORD);
    await wrapper.find('[data-test-id~="loginForm"]').trigger("submit");
    await flushPromises();
    expect(axiosPostMock).toHaveBeenCalledWith(`${BASE_REST_URL}/login`, WRONG_ADMIN_FORMDATA);
    expect((wrapper.vm as any).failed).toEqual(true);
    expect((wrapper.vm as any).isLogining).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("Failed");
    const authenticationStore = useAuthenticationStore();
    expect(authenticationStore.getRedirectUrl()).toEqual(SOME_REDIRECT_URL);
    expect(routerPushMock).not.toHaveBeenCalled();
  });
});
