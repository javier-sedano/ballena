import { type NavigationGuardReturn, type RouteLocationNormalized } from "vue-router";
import { useAuthenticationStore } from "./authentication.store";

const LOGIN_URL = "/login";

function isXGuard(
  to: RouteLocationNormalized,
  _from: RouteLocationNormalized,
  checker: () => boolean,
): NavigationGuardReturn {
  if (!checker()) {
    console.warn("Navigation rejected to ", to.path);
    useAuthenticationStore().setRedirectUrl(to.path);
    return LOGIN_URL;
  }
}

export function isUserGuard(to: RouteLocationNormalized, from: RouteLocationNormalized): NavigationGuardReturn {
  return isXGuard(to, from, useAuthenticationStore().isUser);
}

export function isAdminGuard(to: RouteLocationNormalized, from: RouteLocationNormalized): NavigationGuardReturn {
  return isXGuard(to, from, useAuthenticationStore().isAdmin);
}
