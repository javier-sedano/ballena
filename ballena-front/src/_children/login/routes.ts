import { type RouteRecordRaw } from "vue-router";

export const routes: RouteRecordRaw = {
  path: "/login",
  name: "Login",
    /* v8 ignore next 1 */
    component: () => import("./Login.vue"),
};
