import { defineStore } from "pinia";
import { ref } from "vue";
import { ROLE_ADMIN, ROLE_USER, type User } from "./User";
import { clearStoredLoggedUser, getStoredLoggedUser, storeLoggedUser } from "./loggedUserLocalStorer";
import { authenticationService } from "./authentication.service";
import { versionService } from "../../version.service";

const DEFAULT_REDIRECT_URL = "/";

async function refreshCsrfCookie() {
  /*
  According to Spring Security, we need to refresh the XSRF token ater login and after logout.
  https://docs.spring.io/spring-security/reference/servlet/exploits/csrf.html#csrf-integration-javascript-spa
  Most of the time we do not need it, because after a login /notes/ will load the list of notes... but strictly, we must do it.
  The token is in a cookie, so making any GET request is enough.
  */
 await versionService.getVersion();
}

export const useAuthenticationStore = defineStore("authentication", () => {
  const loggedUser = ref<User | null>(null);
  const redirectUrl = ref<string>(DEFAULT_REDIRECT_URL);

  async function login(username: string, password: string): Promise<void> {
    const user: User = await authenticationService.login(username, password);
    loggedUser.value = user;
    storeLoggedUser(user);
    await refreshCsrfCookie();
  }

  async function logout(): Promise<void> {
    loggedUser.value = null;
    clearStoredLoggedUser();
    await authenticationService.logout();
    await refreshCsrfCookie();
  }

  function refresh(): void {
    loggedUser.value = getStoredLoggedUser();
  }

  function isAuthenticated(): boolean {
    return loggedUser.value !== null;
  }

  function isAdmin(): boolean {
    return loggedUser.value !== null && loggedUser.value.roles.includes(ROLE_ADMIN); // NOSONAR
  }

  function isUser(): boolean {
    return loggedUser.value !== null && loggedUser.value.roles.includes(ROLE_USER); // NOSONAR
  }

  function username(): string {
    return `${loggedUser.value?.username}`;
  }

  function getRedirectUrl(): string {
    return redirectUrl.value;
  }

  function setRedirectUrl(url: string): void {
    redirectUrl.value = url;
  }

  function clearRedirectUrl(): void {
    setRedirectUrl(DEFAULT_REDIRECT_URL);
  }

  return {
    loggedUser,
    login,
    logout,
    refresh,
    isAuthenticated,
    isAdmin,
    isUser,
    username,
    getRedirectUrl,
    setRedirectUrl,
    clearRedirectUrl,
  };
});
