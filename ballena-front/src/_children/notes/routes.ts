import { type RouteRecordRaw } from "vue-router";

import { isUserGuard } from "../login/guards";

export const routes: RouteRecordRaw = {
  path: "/notes/",
  name: "Notes",
  /* v8 ignore next 1 */
  component: () => import("./Notes.vue"),
  beforeEnter: isUserGuard,
  children: [
    {
      path: "",
      name: "NoteNone",
      /* v8 ignore next 1 */
      component: () => import("../../NoneDetails.vue"),
    },
    {
      path: ":id",
      name: "NoteDetails",
      /* v8 ignore next 1 */
      component: () => import("./NoteDetails.vue"),
    },
  ],
};
