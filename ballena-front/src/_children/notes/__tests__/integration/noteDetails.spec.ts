import { describe, it, expect, afterEach, vi } from "vitest";
import { mount, flushPromises, VueWrapper } from "@vue/test-utils";
import { type RouteLocationNormalizedLoadedGeneric, type Router } from "vue-router";
import { mockI18n, mockAxios, BASE_REST_URL, mockRouter, SHOWN_EVENT } from "../../../../__tests__/test.utils";
import { type DetailedNote } from "../../notes.service";
import NoteDetails from "../../NoteDetails.vue";

mockI18n();
const { useRouteMock, mockRouterPush } = mockRouter();
const routerPushMock = mockRouterPush();
const { axiosGetMock, axiosPutMock, axiosDeleteMock } = mockAxios();

afterEach(() => {
  vi.clearAllMocks();
});

describe("Note details screen", async () => {
  const NOTE: DetailedNote = {
    id: 1,
    title: "Title1",
    content: "Content1",
    version: 1,
  };
  const NOTES_REST_URL = `${BASE_REST_URL}/notes`;
  const UPDATED_EVENT = "updated";

  async function createWrapper(): Promise<VueWrapper> {
    useRouteMock.mockReturnValue({ params: { id: NOTE.id } } as unknown as RouteLocationNormalizedLoadedGeneric);
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: NOTE }));
    const wrapper = mount(NoteDetails, {
      global: {
        stubs: {
          "font-awesome-icon": { template: "<span font-awesome-icon/>" },
        },
      },
    });
    expect(wrapper.element).toMatchSnapshot("Initial");
    await flushPromises();
    expect(axiosGetMock).toHaveBeenCalledWith(`${NOTES_REST_URL}/${NOTE.id}`);
    expect((wrapper.vm as any).note).toEqual(NOTE);
    expect(wrapper.emitted()).toHaveProperty(SHOWN_EVENT);
    expect(wrapper.element).toMatchSnapshot("Loaded");
    return wrapper;
  }

  it("Should render", async () => {
    await createWrapper();
  });

  const MODIFIED_NOTE: DetailedNote = {
    id: 1,
    title: "SavedTitle1",
    content: "SavedContent1",
    version: 1,
  };

  it("Should save a note", async () => {
    const SAVED_NOTE: DetailedNote = {
      ...MODIFIED_NOTE,
      version: 2,
    };
    const wrapper = await createWrapper();
    wrapper.find('[data-test-id~="titleField"]').setValue(MODIFIED_NOTE.title);
    wrapper.find('[data-test-id~="contentField"]').setValue(MODIFIED_NOTE.content);
    expect((wrapper.vm as any).isSaving).toEqual(false);
    axiosPutMock.mockImplementationOnce(() => Promise.resolve({ data: SAVED_NOTE }));
    wrapper.find('[data-test-id~="saveButton"]').trigger("click");
    expect(axiosPutMock).toHaveBeenCalledWith(NOTES_REST_URL, MODIFIED_NOTE);
    expect((wrapper.vm as any).isSaving).toEqual(true);
    await flushPromises();
    expect((wrapper.vm as any).isSaving).toEqual(false);
    expect((wrapper.vm as any).note).toEqual(SAVED_NOTE);
    expect(wrapper.emitted()).toHaveProperty(UPDATED_EVENT);
    expect(wrapper.element).toMatchSnapshot("Saved");
    return wrapper;
  });

  it("Should detect fail to save a note", async () => {
    const ERROR_SAVING = "Failed to save";
    const wrapper = await createWrapper();
    wrapper.find('[data-test-id~="titleField"]').setValue(MODIFIED_NOTE.title);
    wrapper.find('[data-test-id~="contentField"]').setValue(MODIFIED_NOTE.content);
    expect((wrapper.vm as any).isSaving).toEqual(false);
    axiosPutMock.mockImplementationOnce(() => Promise.reject(new Error(ERROR_SAVING)));
    await expect(() => (wrapper.vm as any).save()).rejects.toThrowError(ERROR_SAVING);
    expect(axiosPutMock).toHaveBeenCalledWith(NOTES_REST_URL, MODIFIED_NOTE);
    expect((wrapper.vm as any).isSaving).toEqual(false);
    expect((wrapper.vm as any).note).toEqual(MODIFIED_NOTE);
    expect(wrapper.emitted()).not.toHaveProperty(UPDATED_EVENT);
    expect(wrapper.element).toMatchSnapshot("Not saved");
  });

  it("Should delete a note", async () => {
    const wrapper = await createWrapper();
    expect((wrapper.vm as any).isDeleting).toEqual(false);
    axiosDeleteMock.mockImplementationOnce(() => Promise.resolve());
    wrapper.find('[data-test-id~="deleteButton"]').trigger("click");
    expect(axiosDeleteMock).toHaveBeenCalledWith(`${NOTES_REST_URL}/${NOTE.id}`);
    expect((wrapper.vm as any).isDeleting).toEqual(true);
    await flushPromises();
    expect((wrapper.vm as any).isDeleting).toEqual(false);
    expect(wrapper.emitted()).toHaveProperty(UPDATED_EVENT);
    expect(routerPushMock).toHaveBeenCalledWith({ name: "NoteNone" });
    expect(wrapper.element).toMatchSnapshot("Deleted");
    return wrapper;
  });

  it("Should detect fail to delete a note", async () => {
    const ERROR_DELETING = "Failed to delete";
    const wrapper = await createWrapper();
    expect((wrapper.vm as any).isDeleting).toEqual(false);
    axiosDeleteMock.mockImplementationOnce(() => Promise.reject(new Error(ERROR_DELETING)));
    await expect(() => (wrapper.vm as any).deletee()).rejects.toThrowError(ERROR_DELETING);
    expect(axiosDeleteMock).toHaveBeenCalledWith(`${NOTES_REST_URL}/${NOTE.id}`);
    expect((wrapper.vm as any).isDeleting).toEqual(false);
    expect(wrapper.emitted()).not.toHaveProperty(UPDATED_EVENT);
    expect(wrapper.element).toMatchSnapshot("Not deleted");
    return wrapper;
  });
});
