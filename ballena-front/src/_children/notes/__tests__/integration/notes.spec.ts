import { describe, it, expect, afterEach, vi } from "vitest";
import { mount, flushPromises, VueWrapper } from "@vue/test-utils";
import { type RouteLocationNormalizedLoadedGeneric } from "vue-router";
import { mockI18n, mockAxios, BASE_REST_URL, mockRouter } from "../../../../__tests__/test.utils";
import Notes from "../../Notes.vue";
import { type MinimalNote } from "../../../notes/notes.service";

mockI18n();
const { useRouteMock, mockRouterPush } = mockRouter();
const routerPushMock = mockRouterPush();
const { axiosGetMock, axiosPostMock } = mockAxios();

afterEach(() => {
  vi.clearAllMocks();
});

describe("Notes screen", async () => {
  const MINIMAL_NOTE1: MinimalNote = {
    id: 1,
    title: "Title1",
  };
  const MINIMAL_NOTE2: MinimalNote = {
    id: 2,
    title: "Title2",
  };
  const MINIMAL_NOTES: MinimalNote[] = [
    MINIMAL_NOTE1,
    MINIMAL_NOTE2,
  ];
  const NOTES_REST_URL = `${BASE_REST_URL}/notes`;

  async function createWrapper(): Promise<VueWrapper> {
    useRouteMock.mockReturnValue({ fullPath: "/notes/" } as RouteLocationNormalizedLoadedGeneric);
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: MINIMAL_NOTES }));
    const wrapper = mount(Notes, {
      global: {
        stubs: {
          "router-link": { template: "<span router-link><slot/></span>" },
          "router-view": { template: "<span router-view/>" },
          "font-awesome-icon": { template: "<span font-awesome-icon/>" },
        },
      },
      attachTo: document.body,
    });
    expect((wrapper.vm as any).isFinding).toEqual(true);
    expect(wrapper.element).toMatchSnapshot("Initial");
    await flushPromises();
    expect(axiosGetMock).toHaveBeenCalledWith(NOTES_REST_URL, { params: { filter: "", from: 0 } });
    expect((wrapper.vm as any).isFinding).toEqual(false);
    expect((wrapper.vm as any).notes).toEqual(MINIMAL_NOTES);
    expect(wrapper.element).toMatchSnapshot("Loaded");
    return wrapper;
  }
  it("Should render", async () => {
    await createWrapper();
  });
  async function testFilter(action: (wrapper: VueWrapper) => Promise<void>): Promise<VueWrapper> {
    const FILTER = "1";
    const wrapper = await createWrapper();
    axiosGetMock.mockImplementationOnce(() =>
      Promise.resolve({
        data: [
          MINIMAL_NOTE1,
        ],
      }),
    );
    wrapper.find('[data-test-id~="filterField"]').setValue(FILTER);
    await action(wrapper);
    await flushPromises();
    expect(axiosGetMock).toHaveBeenCalledWith(NOTES_REST_URL, { params: { filter: FILTER, from: 0 } });
    expect((wrapper.vm as any).notes).toEqual([
      MINIMAL_NOTE1,
    ]);
    return wrapper;
  }
  it("Should filter notes", async () => {
    const wrapper = await testFilter(async (wrapper) => {
      await wrapper.find('[data-test-id~="filterField"]').trigger("keyup");
    });
    expect(wrapper.element).toMatchSnapshot("Filtered");
  });
  it("Should refresh search", async () => {
    const wrapper = await testFilter(async (wrapper) => {
      await wrapper.find('[data-test-id~="findButton"]').trigger("click");
    });
    expect(wrapper.element).toMatchSnapshot("Filtered");
  });
  it("Should create a new note", async () => {
    const MINIMAL_NOTE3: MinimalNote = {
      id: 3,
      title: "Title3",
    };
    const NEW_MINIMAL_NOTES: MinimalNote[] = [
      ...MINIMAL_NOTES,
      MINIMAL_NOTE3,
    ];
    const wrapper = await createWrapper();
    axiosPostMock.mockImplementationOnce(() => Promise.resolve({ data: MINIMAL_NOTE3.id }));
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: NEW_MINIMAL_NOTES }));
    expect((wrapper.vm as any).isAdding).toEqual(false);
    await wrapper.find('[data-test-id~="addButton"]').trigger("click");
    expect((wrapper.vm as any).isAdding).toEqual(true);
    await flushPromises();
    expect((wrapper.vm as any).isAdding).toEqual(false);
    expect(axiosPostMock).toHaveBeenCalledWith(NOTES_REST_URL);
    expect(axiosGetMock).toHaveBeenCalledWith(NOTES_REST_URL, { params: { filter: "", from: 0 } });
    expect((wrapper.vm as any).notes).toEqual(NEW_MINIMAL_NOTES);
    expect(routerPushMock).toHaveBeenCalledWith({ name: "NoteDetails", params: { id: MINIMAL_NOTE3.id } });
    expect(wrapper.element).toMatchSnapshot("Created");
  });
  it("Should select a note", async () => {
    const SELECTED_NOTE_ID = 1;
    const wrapper = await createWrapper();
    await (wrapper.vm as any).selectNote(SELECTED_NOTE_ID);
    flushPromises();
    expect((wrapper.vm as any).selectedNoteId).toEqual(SELECTED_NOTE_ID);
    expect((wrapper.vm as any).showSmallSearchPanel).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("Note selected");
  });
  it("Should show and hide the search panel", async () => {
    const wrapper = await createWrapper();
    expect(wrapper.find('[data-test-id~="smallSearchPanel"]').isVisible()).toEqual(false);
    await wrapper.find('[data-test-id~="showSmallSearchButton"]').trigger("click");
    expect(wrapper.find('[data-test-id~="smallSearchPanel"]').isVisible()).toEqual(true);
    await wrapper.find('[data-test-id~="hideSmallSearchButton"]').trigger("click");
    expect(wrapper.find('[data-test-id~="smallSearchPanel"]').isVisible()).toEqual(false);
  });
});
