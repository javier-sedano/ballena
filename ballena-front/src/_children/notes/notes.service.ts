import axios from "axios";
import { REST_BASE_URL } from "../../service.constants";

const URL = `${REST_BASE_URL}/notes`;

export interface MinimalNote {
  id: number;
  title: string;
}

export interface DetailedNote {
  id: number;
  title: string;
  content: string;
  version: number;
}

export const notesService = {
  findNotes: async (filter: string, from: number): Promise<MinimalNote[]> => {
    const response = await axios.get<MinimalNote[]>(URL, { params: { filter, from } });
    return response.data;
  },
  getNote: async (id: number): Promise<DetailedNote> => {
    const response = await axios.get<DetailedNote>(`${URL}/${id}`);
    return response.data;
  },
  updateNote: async (note: DetailedNote): Promise<DetailedNote> => {
    const response = await axios.put<DetailedNote>(URL, note);
    return response.data;
  },
  deleteNote: async (id: number): Promise<void> => {
    await axios.delete<void>(`${URL}/${id}`);
  },
  createNote: async (): Promise<number> => {
    const response = await axios.post<number>(URL);
    return response.data;
  },
};
