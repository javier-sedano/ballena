export type concurrentFunctionType = () => Promise<void>;

export function unconcurrent(concurrentFunction: concurrentFunctionType) {
  let next: concurrentFunctionType | null;
  let running: boolean = false;
  return async function (): Promise<void> {
    next = concurrentFunction;
    if (running) {
      console.debug("Delaying running of ", next);
      return;
    }
    running = true;
    try {
      while (next !== null) {
        const unconcurrentFunction: concurrentFunctionType = next;
        next = null;
        console.debug("Running ", unconcurrentFunction);
        await unconcurrentFunction();
      }
    } finally {
      running = false;
    }
  };
}
