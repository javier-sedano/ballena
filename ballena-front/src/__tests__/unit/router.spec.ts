import { describe, it, expect } from "vitest";
import { type RouteRecordRaw } from "vue-router";
import { router } from "../../router";

describe("Router", () => {
  it("Should create", () => {
    expect(router).toBeDefined();
  });

  it("Should load lazy routes", () => {
    function loadLazyRoutes(routes: Readonly<RouteRecordRaw[]>) {
      routes.forEach((route: RouteRecordRaw) => {
        if (route.children !== undefined) {
          loadLazyRoutes(route.children);
        }
      });
    }
    loadLazyRoutes(router.options.routes);
    expect(router).toBeDefined();
  });
});
