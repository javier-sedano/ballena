import { describe, it, expect, beforeEach, afterEach, vi } from "vitest";
import { shallowMount } from "@vue/test-utils";
import { setActivePinia, createPinia } from "pinia";
import { mockI18n } from "../test.utils";
import AppMenu from "../../AppMenu.vue";

mockI18n();

beforeEach(() => {
  setActivePinia(createPinia());
});

afterEach(() => {
  vi.clearAllMocks();
});

describe("AppMenu.vue", async () => {
  it("Should render empty", async () => {
    const wrapper = shallowMount(AppMenu, {
      global: {
        stubs: {
          "router-link": { template: "<router-link-stub><slot/></router-link-stub>" },
          "router-view": { template: "<router-view-stub/>" },
          "font-awesome-icon": { template: "<font-awesome-icon-stub/>" },
        },
      },
      attachTo: document.body,
    });
    expect(wrapper.element).toMatchSnapshot("Initial");
  });
});
