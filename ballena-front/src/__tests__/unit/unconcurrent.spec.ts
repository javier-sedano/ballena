import { describe, it, expect } from "vitest";
import { sleep } from "../test.utils";
import { unconcurrent } from "../../unconcurrent";

describe("unconcurrent", () => {
  it("should run the function if called once", async () => {
    let started: number[] = [];
    let finished: number[] = [];
    let input: number = 0;
    async function concurrentSquare(): Promise<void> {
      const i: number = input;
      started.push(i);
      await sleep(0);
      finished.push(i);
    }
    const unconcurrentSquare = unconcurrent(concurrentSquare);
    input = 1;
    const unconcurrentSquarePromise = unconcurrentSquare();
    expect(started).toEqual([
      1,
    ]);
    expect(finished).toEqual([]);
    await unconcurrentSquarePromise;
    expect(started).toEqual([
      1,
    ]);
    expect(finished).toEqual([
      1,
    ]);
  });

  it("should run the second function later if called concurrently", async () => {
    let started: number[] = [];
    let finished: number[] = [];
    let input: number = 0;
    async function concurrentSquare(): Promise<void> {
      const i: number = input;
      started.push(i);
      await sleep(0);
      finished.push(i);
    }
    const unconcurrentSquare = unconcurrent(concurrentSquare);
    input = 1;
    const unconcurrentSquarePromise1 = unconcurrentSquare();
    expect(started).toEqual([
      1,
    ]);
    expect(finished).toEqual([]);
    input = 2;
    const unconcurrentSquarePromise2 = unconcurrentSquare();
    expect(started).toEqual([
      1,
    ]);
    expect(finished).toEqual([]);
    await unconcurrentSquarePromise1;
    await unconcurrentSquarePromise2;
    expect(started).toEqual([
      1,
      2,
    ]);
    expect(finished).toEqual([
      1,
      2,
    ]);
  });

  it("should run the third function latter, skipping the second, if called concurrently", async () => {
    let started: number[] = [];
    let finished: number[] = [];
    let input: number = 0;
    async function concurrentSquare(): Promise<void> {
      const i: number = input;
      started.push(i);
      await sleep(0);
      finished.push(i);
    }
    const unconcurrentSquare = unconcurrent(concurrentSquare);
    input = 1;
    const unconcurrentSquarePromise1 = unconcurrentSquare();
    expect(started).toEqual([
      1,
    ]);
    expect(finished).toEqual([]);
    input = 2;
    const unconcurrentSquarePromise2 = unconcurrentSquare();
    expect(started).toEqual([
      1,
    ]);
    expect(finished).toEqual([]);
    input = 3;
    const unconcurrentSquarePromise3 = unconcurrentSquare();
    expect(started).toEqual([
      1,
    ]);
    expect(finished).toEqual([]);
    await unconcurrentSquarePromise1;
    await unconcurrentSquarePromise2;
    await unconcurrentSquarePromise3;
    expect(started).toEqual([
      1,
      3,
    ]);
    expect(finished).toEqual([
      1,
      3,
    ]);
  });

  it("should run te fourth function if called after finish", async () => {
    let started: number[] = [];
    let finished: number[] = [];
    let input: number = 0;
    async function concurrentSquare(): Promise<void> {
      const i: number = input;
      started.push(i);
      await sleep(0);
      finished.push(i);
    }
    const unconcurrentSquare = unconcurrent(concurrentSquare);
    input = 1;
    const unconcurrentSquarePromise1 = unconcurrentSquare();
    expect(started).toEqual([
      1,
    ]);
    expect(finished).toEqual([]);
    input = 2;
    const unconcurrentSquarePromise2 = unconcurrentSquare();
    expect(started).toEqual([
      1,
    ]);
    expect(finished).toEqual([]);
    input = 3;
    const unconcurrentSquarePromise3 = unconcurrentSquare();
    expect(started).toEqual([
      1,
    ]);
    expect(finished).toEqual([]);
    await unconcurrentSquarePromise1;
    await unconcurrentSquarePromise2;
    await unconcurrentSquarePromise3;
    expect(started).toEqual([
      1,
      3,
    ]);
    expect(finished).toEqual([
      1,
      3,
    ]);
    input = 4;
    const unconcurrentSquarePromise4 = unconcurrentSquare();
    expect(started).toEqual([
      1,
      3,
      4,
    ]);
    expect(finished).toEqual([
      1,
      3,
    ]);
    await unconcurrentSquarePromise4;
    expect(started).toEqual([
      1,
      3,
      4,
    ]);
    expect(finished).toEqual([
      1,
      3,
      4,
    ]);
  });
});
