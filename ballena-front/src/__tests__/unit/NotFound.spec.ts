import { describe, it, expect } from "vitest";
import { shallowMount } from "@vue/test-utils";
import { mockI18n } from "../test.utils";
import NotFound from "../../NotFound.vue";

mockI18n();

describe("NotFound.vue", async () => {
  it("Should render empty", async () => {
    const wrapper = shallowMount(NotFound);
    expect(wrapper.element).toMatchSnapshot("Initial");
  });
});
