import { describe, it, expect } from "vitest";
import { flushPromises, shallowMount } from "@vue/test-utils";
import { mockI18n, SHOWN_EVENT } from "../test.utils";
import NoneDetails from "../../NoneDetails.vue";

mockI18n();

describe("NotFound.vue", async () => {
  it("Should render empty", async () => {
    const wrapper = shallowMount(NoneDetails);
    await flushPromises();
    expect(wrapper.emitted()).toHaveProperty(SHOWN_EVENT);
    expect(wrapper.element).toMatchSnapshot("Initial");
  });
});
