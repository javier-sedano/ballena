import { vi, type Mock, type MockedFunction } from "vitest";
import axios from "axios";
import { useRoute, useRouter, type Router } from "vue-router";
import { type User } from "../_children/login/User";

export const APP_LANG_LOCALSTORAGE_KEY = "ballenaLang";
export const LOCALE_ENGLISH = "en";
export const LOCALE_SPANISH = "es";
export const localeMock = {
  value: LOCALE_ENGLISH,
};
export function mockI18n() {
  vi.mock("vue-i18n", async () => {
    const actual = await vi.importActual("vue-i18n");
    return {
      ...actual,
      useI18n: () => ({
        t: (key: string) => key,
        locale: localeMock,
      }),
    };
  });
}

export const BASE_REST_URL = "/ballena/rest";
export function mockAxios(): {
  axiosGetMock: MockedFunction<typeof axios.get>;
  axiosPostMock: MockedFunction<typeof axios.post>;
  axiosPutMock: MockedFunction<typeof axios.put>;
  axiosDeleteMock: MockedFunction<typeof axios.delete>;
} {
  vi.mock("axios", () => {
    return {
      default: {
        get: vi.fn(),
        post: vi.fn(),
        put: vi.fn(),
        delete: vi.fn(),
      },
    };
  });
  const axiosGetMock = axios.get as MockedFunction<typeof axios.get>;
  const axiosPostMock = axios.post as MockedFunction<typeof axios.post>;
  const axiosPutMock = axios.put as MockedFunction<typeof axios.put>;
  const axiosDeleteMock = axios.delete as MockedFunction<typeof axios.delete>;
  return {
    axiosGetMock,
    axiosPostMock,
    axiosPutMock,
    axiosDeleteMock,
  };
}

export const APP_LOGGEDUSER_LOCALSTORAGE_KEY = "ballenaLoggedUser";
export const APP_LOGGEDUSER_VERSION = 1;

export const ROLE_USER = "ROLE_USER";
export const ROLE_ADMIN = "ROLE_ADMIN";
export const USER_USERNAME = "User";
export const USER_PASSWORD = "U53r";
export const USER_USER: User = {
  username: USER_USERNAME,
  roles: [
    ROLE_USER,
  ],
};
export const USER_FORMDATA = new FormData();
USER_FORMDATA.append("username", USER_USERNAME);
USER_FORMDATA.append("password", USER_PASSWORD);
export const ADMIN_USERNAME = "Admin";
export const ADMIN_PASSWORD = "4dm1n";
export const ADMIN_USER: User = {
  username: ADMIN_USERNAME,
  roles: [
    ROLE_USER,
    ROLE_ADMIN,
  ],
};
export const ADMIN_FORMDATA = new FormData();
ADMIN_FORMDATA.append("username", ADMIN_USERNAME);
ADMIN_FORMDATA.append("password", ADMIN_PASSWORD);

export const VERSION = "42";

export function mockRouter() {
  vi.mock("vue-router", () => ({
    useRoute: vi.fn(),
    useRouter: vi.fn(),
  }));
  const useRouteMock = useRoute as MockedFunction<typeof useRoute>;
  const useRouterMock = useRouter as MockedFunction<typeof useRouter>;
  const mockRouterPush = (): Mock => {
    const routerPushMock = vi.fn();
    useRouterMock.mockReturnValue({ push: routerPushMock } as unknown as Router);
    return routerPushMock;
  };
  return {
    useRouteMock,
    useRouterMock,
    mockRouterPush,
  };
}

export const SHOWN_EVENT = "shown";

export async function sleep(ms: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
