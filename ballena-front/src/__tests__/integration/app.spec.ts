import { describe, it, expect, beforeEach, afterEach, vi } from "vitest";
import { mount, flushPromises, VueWrapper } from "@vue/test-utils";
import { setActivePinia, createPinia } from "pinia";
import {
  mockI18n,
  localeMock,
  LOCALE_ENGLISH,
  LOCALE_SPANISH,
  mockAxios,
  BASE_REST_URL,
  APP_LOGGEDUSER_LOCALSTORAGE_KEY,
  APP_LOGGEDUSER_VERSION,
  APP_LANG_LOCALSTORAGE_KEY,
  VERSION,
  USER_USER,
  USER_USERNAME,
  USER_PASSWORD,
  ADMIN_USER,
  ADMIN_USERNAME,
  ADMIN_PASSWORD,
  mockRouter,
} from "../test.utils";
import App from "../../App.vue";
import { type User } from "../../_children/login/User";
import { useAuthenticationStore } from "../../_children/login/authentication.store";
import { useErrorStore } from "../../error/error.store";

mockI18n();
const { mockRouterPush } = mockRouter();
const routerPushMock = mockRouterPush();
const { axiosGetMock, axiosPostMock } = mockAxios();

beforeEach(() => {
  setActivePinia(createPinia());
  localStorage.removeItem(APP_LOGGEDUSER_LOCALSTORAGE_KEY);
});

afterEach(() => {
  vi.clearAllMocks();
});

describe("App container", async () => {
  function createWrapper(): VueWrapper {
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: VERSION }));
    const wrapper = mount(App, {
      global: {
        stubs: {
          "router-link": { template: "<span router-link><slot/></span>" },
          "router-view": { template: "<span router-view/>" },
          "font-awesome-icon": { template: "<span font-awesome-icon/>" },
        },
      },
      attachTo: document.body,
    });
    expect(axiosGetMock).toHaveBeenCalledWith(`${BASE_REST_URL}/ping/ver`);
    axiosGetMock.mockClear();
    return wrapper;
  }
  async function createWrapperWithUser(user: User, username: string, password: string): Promise<VueWrapper> {
    const authenticationStore = useAuthenticationStore();
    axiosPostMock.mockImplementationOnce(() => Promise.resolve({ data: user }));
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: VERSION }));
    await authenticationStore.login(username, password);
    const wrapper = createWrapper();
    await flushPromises();
    expect(wrapper.find('[data-test-id~="appBar"]').exists()).toEqual(true);
    expect(wrapper.find('[data-test-id~="version"]').element.getAttribute("title")).toEqual(VERSION);
    axiosPostMock.mockClear();
    axiosGetMock.mockClear();
    return wrapper;
  }
  it("Should render", async () => {
    const wrapper = createWrapper();
    const authenticationStore = useAuthenticationStore();
    expect(authenticationStore.loggedUser).toBeNull();
    expect(wrapper.find('[data-test-id~="appBar"]').exists()).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("Initial");
  });
  it("Should render for plain user", async () => {
    const wrapper = await createWrapperWithUser(USER_USER, USER_USERNAME, USER_PASSWORD);
    expect(wrapper.element).toMatchSnapshot("Plain user");
  });
  it("Should render for admin", async () => {
    const wrapper = await createWrapperWithUser(ADMIN_USER, ADMIN_USERNAME, ADMIN_PASSWORD);
    expect(wrapper.element).toMatchSnapshot("Admin");
  });
  it("Should show and hide the menu", async () => {
    const wrapper = await createWrapperWithUser(ADMIN_USER, ADMIN_USERNAME, ADMIN_PASSWORD);
    const burgerButton = wrapper.find('[data-test-id~="burgerButton"]');
    const smallMenu = wrapper.find('[data-test-id~="smallMenu"]');
    expect(smallMenu.isVisible()).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("Before toggle menu");
    await burgerButton.trigger("click");
    expect(smallMenu.isVisible()).toEqual(true);
    expect(wrapper.element).toMatchSnapshot("After show menu");
    await smallMenu.trigger("click");
    expect(smallMenu.isVisible()).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("After hide menu");
    await burgerButton.trigger("click");
    expect(smallMenu.isVisible()).toEqual(true);
    expect(wrapper.element).toMatchSnapshot("After show menu");
    await burgerButton.trigger("click");
    expect(smallMenu.isVisible()).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("After hide menu");
  });
  it("Should set the language", async () => {
    const wrapper = await createWrapperWithUser(ADMIN_USER, ADMIN_USERNAME, ADMIN_PASSWORD);
    expect(localStorage.getItem(APP_LANG_LOCALSTORAGE_KEY)).toEqual(null);
    expect(localeMock.value).toEqual(LOCALE_ENGLISH);
    await wrapper.find('[data-test-id~="esButton"]').trigger("click");
    expect(localStorage.getItem(APP_LANG_LOCALSTORAGE_KEY)).toEqual("es");
    expect(localeMock.value).toEqual(LOCALE_SPANISH);
    await wrapper.find('[data-test-id~="enButton"]').trigger("click");
    expect(localStorage.getItem(APP_LANG_LOCALSTORAGE_KEY)).toEqual("en");
    expect(localeMock.value).toEqual(LOCALE_ENGLISH);
  });
  it("Should logout", async () => {
    const wrapper = await createWrapperWithUser(ADMIN_USER, ADMIN_USERNAME, ADMIN_PASSWORD);
    const expectedLoggedUserJson = JSON.stringify({ ...ADMIN_USER, version: APP_LOGGEDUSER_VERSION });
    expect(localStorage.getItem(APP_LOGGEDUSER_LOCALSTORAGE_KEY)).toEqual(expectedLoggedUserJson);
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: VERSION }));
    await wrapper.find('[data-test-id~="logoutButton"]').trigger("click");
    const authenticationStore = useAuthenticationStore();
    expect(authenticationStore.loggedUser).toBeNull();
    expect(localStorage.getItem(APP_LOGGEDUSER_LOCALSTORAGE_KEY)).toBeNull();
    expect(axiosPostMock).toHaveBeenCalledWith(`${BASE_REST_URL}/logout`);
    expect(axiosGetMock).toHaveBeenCalledWith(`${BASE_REST_URL}/ping/ver`);
    expect(routerPushMock).toHaveBeenCalledWith("/login");
  });
  it("should show error when stored and dismiss", async () => {
    const AN_ERROR = "An error";
    const errorStore = useErrorStore();
    const wrapper = createWrapper();
    console.log("errorDialog", wrapper.find('[data-test-id~="errorDialog"]'));
    expect(wrapper.find('[data-test-id~="errorDialog"]').exists()).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("Initial");
    errorStore.setErrorMessage(AN_ERROR);
    await flushPromises();
    expect(wrapper.find('[data-test-id~="errorDialog"]').exists()).toEqual(true);
    expect(wrapper.find('[data-test-id~="errorMessage"]').text()).toEqual(AN_ERROR);
    expect(wrapper.element).toMatchSnapshot("Showing");
    await wrapper.find('[data-test-id~="okButton"]').trigger("click");
    expect(wrapper.find('[data-test-id~="errorDialog"]').exists()).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("Initial");
  });
});
