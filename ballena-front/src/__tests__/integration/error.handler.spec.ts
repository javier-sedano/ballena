import { describe, it, expect, beforeEach, afterEach, vi } from "vitest";
import { setActivePinia, createPinia } from "pinia";
import { type Router } from "vue-router";
import { mockI18n, mockAxios, VERSION } from "../test.utils";
import { type I18nType } from "../../i18n/i18n";
import { useErrorStore } from "../../error/error.store";
import { setupErrorHandler } from "../../error/error.handler";

const routerMock = {
  push: vi.fn(),
} as unknown as Router;
const i18nMock = {
  global: {
    t: (msg: string, args: any): string => {
      return msg + (args ? " | " + JSON.stringify(args) : "");
    },
  },
} as unknown as I18nType;
const consoleErrorSpy = vi.spyOn(console, "error");
mockI18n();
const { axiosGetMock, axiosPostMock } = mockAxios();
const errorHandler = setupErrorHandler(routerMock, i18nMock);

afterEach(() => {
  vi.clearAllMocks();
});

beforeEach(() => {
  setActivePinia(createPinia());
});

describe("error handler", () => {
  it("should forward auth errors to login", () => {
    const authError = { status: 401 };
    axiosPostMock.mockImplementationOnce(() => Promise.resolve());
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: VERSION }));
    errorHandler(authError, null, "");
    expect(routerMock.push).toHaveBeenCalledWith({ name: "Login" });
  });
  it("should store undefined error", () => {
    const errorStore = useErrorStore();
    errorHandler(undefined, null, "");
    expect(errorStore.errorMessage).toEqual("Unknown error");
    expect(consoleErrorSpy).toHaveBeenCalledWith(undefined);
  });
  it("should store plain error with vm.$t", () => {
    const AN_ERROR_MESSAGE = "An error message";
    const PLAIN_ERROR = { message: AN_ERROR_MESSAGE };
    const errorStore = useErrorStore();
    errorHandler(PLAIN_ERROR, null, "");
    expect(errorStore.errorMessage).toEqual('error.i18nCode.default | {"message":"' + AN_ERROR_MESSAGE + '"}');
    expect(consoleErrorSpy).toHaveBeenCalledWith(PLAIN_ERROR);
  });
  it("should store controlled error", () => {
    const CONTROLLED_ERROR = {
      response: {
        data: {
          i18nCode: "notes.errorI18nCode.wrong",
          args: { id: 42 },
        },
      },
    };
    const errorStore = useErrorStore();
    errorHandler(CONTROLLED_ERROR, null, "");
    expect(errorStore.errorMessage).toEqual(
      `${CONTROLLED_ERROR.response.data.i18nCode} | {"id":${CONTROLLED_ERROR.response.data.args.id}}`,
    );
    expect(consoleErrorSpy).toHaveBeenCalledWith(CONTROLLED_ERROR);
  });
});
