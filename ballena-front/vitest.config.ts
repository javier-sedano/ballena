import { fileURLToPath } from "node:url";
import { mergeConfig, defineConfig, configDefaults } from "vitest/config";
import viteConfig from "./vite.config";

export default mergeConfig(
  viteConfig,
  defineConfig({
    test: {
      environment: "jsdom",
      exclude: [...configDefaults.exclude, "e2e/**"],
      root: fileURLToPath(new URL("./", import.meta.url)),
      coverage: {
        enabled: true,
        provider: "v8",
        reporter: [
          "lcov",
          "text",
          "html",
        ],
        include: [
          "src/**",
        ],
        exclude: [
          "src/main.ts",
          "**/*.spec.{j,t}s",
          "src/i18n/**",
        ],
        thresholds: {
          branches: 100,
          functions: 100,
          lines: 100,
          statements: 100,
        },
      },
    },
  }),
);
