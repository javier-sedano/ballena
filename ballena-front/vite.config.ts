import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import vueDevTools from "vite-plugin-vue-devtools";

// https://vite.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vueDevTools(),
  ],
  base: "/ballena/web/",
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  build: {
    rollupOptions: {
      output: {
        manualChunks: {
          notFound: [
            "./src/NotFound.vue",
          ],
          noneDetails: [
            "./src/NoneDetails.vue",
          ],
          login: [
            "./src/_children/login/Login.vue",
          ],
          notes: [
            "./src/_children/notes/Notes.vue",
            "./src/_children/notes/NoteDetails.vue",
          ],
          admin: [
            "./src/_children/admin/Admin.vue",
          ],
          system: [
            "./src/_children/admin/_children/system/System.vue",
          ],
          users: [
            "./src/_children/admin/_children/users/Users.vue",
            "./src/_children/admin/_children/users/UserDetails.vue",
          ],
        },
      },
    },
  },
});
