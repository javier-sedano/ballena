// prettier.config.js, .prettierrc.js, prettier.config.mjs, or .prettierrc.mjs

/**
 * @see https://prettier.io/docs/en/configuration.html
 * @type {import("prettier").Config}
 */
const config = {
  plugins: [
    'prettier-plugin-multiline-arrays',
  ],
  multilineArraysWrapThreshold: 0,
};

export default config;
