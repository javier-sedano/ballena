#!/bin/bash
cd "$(dirname "$0")"
TAG=latest
if [ "$1" ]
then
  TAG="$1"
fi
REGISTRY=registry.gitlab.com/javier-sedano/ballena/app
IMAGE=$REGISTRY:$TAG
containerId=`docker ps --filter "name=ballena" --filter "status=running" --quiet`
docker container stop ballena
docker container rm ballena
docker image ls | grep $REGISTRY | while read LINE
do
  OLD_TAG=`echo $LINE | cut -d\  -f2`
  docker image rm $REGISTRY:$OLD_TAG
done
docker pull $IMAGE
docker container create -m 220m -p 5980:8080 -v /data/ballena/:/app/data/ -e spring.flyway.locations=classpath:/db/migration,classpath:/db/sample --restart unless-stopped --name ballena $IMAGE
if [ "$containerId" ]
then
  docker container start ballena
fi
