cd %~dp0
set BASEPATH=%~dp0
powershell -Command "(gc BallenaUpdater_template.xml) -Replace 'BASEPATH', '%BASEPATH%' | Out-File -Encoding ASCII BallenaUpdater.xml"

schtasks /delete /tn "BallenaUpdater" /f
schtasks /create /xml .\BallenaUpdater.xml /tn "BallenaUpdater" /ru "SYSTEM"
