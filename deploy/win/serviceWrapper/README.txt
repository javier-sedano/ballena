Unzip this file to a directory. If you want to preserve data, it is stored in the data/ directory.

Edit ballenaServiceWrapper.xml to fit your needs (actually, it works out of the box) using instructions from https://github.com/kohsuke/winsw

Install: ballenaServiceWrapper install
Start: ballenaServiceWrapper start (or use Windows capabilities)
Stop: ballenaServiceWrapper stop (or use Windows capabilities)
Uninstall: ballenaServiceWrapper uninstall

An autoupdater (for polling-based CD) is included. Use updater_install.bat (and updater_uninstall.bat).
